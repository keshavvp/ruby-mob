package com.example.rubymobile.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SuspendParser extends DefaultHandler {

    StringBuffer buffer = new StringBuffer();
    private boolean isres;
    private boolean isSuccess;
    static  String failurereason;

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {
        buffer.delete(0, buffer.length());
       if(localName.equalsIgnoreCase(VistConstants.RESULT)) {
            isres = true;
        }else if(localName.equalsIgnoreCase(VistConstants.FAILURE)&& isres ==true) {
            isSuccess = false;
            failurereason = attributes.getValue("reason");
        }
        else if(localName.equalsIgnoreCase(VistConstants.SUCCESS)&& isres ==true) {
            isSuccess = true;
        }

    }


}
