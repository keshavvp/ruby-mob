package com.example.rubymobile.parsers;

public class VistConstants {
	public static final String TXN_FREQUENCY = "postFinancialTxnFrequency";
	public static final String CommanderIP = "192.168.31.11";

	public static final String Sellable = "sellableItems";

	public static final String ROVER_NS = "urn:vfi-extpos.2001-10-01";
	public static final String ROVER_NS_PREFIX = "r";

	public static final String SALES_PROCESSOR_CMD = "salesProcessorCmd";
	public static final String EXT_SESSION_ID = "externalSessionIdentifier";
	public static final String CUSTOMER_DOB = "customerDOB";
	public static final String TILL_PROCESSOR_CMD = "tillProcessorCmd";

	public static final String REPRINT_TRANS_RESP = "reprintTransactionResp";
	public static final String TRANSACTION_SEQ = "transactionSeq";
	public static final String REPRINT_TRANS = "reprintTransaction";
	public static final String SUSPEND_TXN = "suspendFinancialTxn";
	public static final String RECALL_TXN = "recallFinancialTxn";
	public static final String GENERATE_STICKY_LABEL = "generateStickyLabels";
	public static final String SAFE_DROP_REQUIRED = "safeDropRequired";
	public static final String OPEN_DRAWER = "openDrawer";

	public static final String FPStateChange = "<FPStateChange";
	public static final String FPStateChangeEnd = "</FPStateChange>";

	public static final String DeviceClass = "DeviceClass";
	public static final String DeviceState = "DeviceState";

	public static final String FDC_READY = "FDC_READY"; //idle
	public static final String FDC_RESERVED  = "FDC_RESERVED"; //reserved  Orange pump
	public static final String FDC_AUTHORIZED = "FDC_AUTHORIZED"; //Dispensing
	public static final String FDC_FUELLING = "FDC_FUELLING"; //Dispensing
	public static final String FDC_CALLING  = "FDC_CALLING"; //Dispensing
	public static final String FDC_DISABLED  = "FDC_DISABLED"; //Dispensing




	//BeginSession
	public static final String EXT_USER_INFO = "externalUserInformation";
	public static final String USER_ID = "userID";
	public static final String USER_PASSWD = "userPasswd";
	public static final String TERMINAL_ID = "terminalIdentifier";
	public static final String TERMINAL_INFO = "terminalInfo";
	public static final String APPLICATION = "application";
	public static final String VERSION = "version";
	public static final String BUILD_DATE = "buildDate";
	public static final String OS_TYPE = "os";
	public static final String DATE_TIME = "dateTime";
	public static final String TILL_STATUS = "tillStatus";
	public static final String OPEN = "OPEN";
	public static final String CLOSE = "CLOSE";
	public static final String IN_USE = "IN_USE";
	public static final String CASHIER_NUM = "cashierNum";
	public static final String COMMUNICATION_MODE = "communicationMode";
	public static final String LAST_TRANSACTION_SEQ = "lastTransactionSeq";
	public static final String STATUS = "status";



	//EvaluateSale
	public static final String EXTERNAL_SALE = "externalSale";
	public static final String ITEM = "item";
	public static final String CHECK_AGE = "checkAge";
	public static final String PREPAY_FUEL = "prepayFuel";
	public static final String POST_PAY_FUEL = "postpayFuel";
	public static final String FUEL_SEQUENCE_NUM = "fuelSequenceNr";
	public static final String AMOUNT_LIMIT = "amountLimit";
	public static final String FACE_AMOUNT = "faceAmount";
	public static final String FACE_VOLUME = "faceVolume";
	public static final String FACE_PPU = "facePPU";
	public static final String FUEL_DEPOSIT = "fuelDeposit";
	public static final String IS_SALE_COMPLETED = "isSaleCompleted";
	public static final String PROMPT = "prompt";
	public static final String TYPE = "type";
	public static final String SCOPE = "scope";
	public static final String INPUT = "input";
	public static final String FIELD = "field";
	public static final String SERVICE_LEVEL = "serviceLevel";
	public static final String OVERRIDEN_PRICE="overriddenPrice";
	public static final String UNIT_PRICE="unitPrice";
	public static final String SUB_ITEM="subItem";
	public static final String ORIG_PRICE="originalPrice";
	public static final String ADJUSTED_PRICE= "adjustedPrice";
	public static final String PRICE_ADJ=  "priceAdjustment";
	public static final String REASON=  "reason";
	public static final String DISCOUNT="discount";
	public static final String FIXED="fixed";
	public static final String MNUAL="manual";
	public static final String PERCENT="percent";
	public static final String AMT="amount";
	public static final String FUEL="fuel";
	public static final String VOLUME="volume";
	public static final String APPLIED="applied";
	public static final String EXTPOS_DOMAIN_NS   = "r";
	public static final String CUSTOMER_NAME   = "customerName";
	public static final String BARCODE = "barcode";
	public static final String DATA = "data";
	public static final String FORMAT = "format";
	public static final String TOTAL_EVENT = "totalEvent";


	//FinalizeSale
	public static final String FINAL_EXTERNAL_SALE = "finalExternalSale";
	public static final String TAX_INDICATOR = "taxIndicator";
	public static final String EXCEPTIONS = "exception";
	public static final String ITEM_INVALID = "itemInvalid";
	public static final String ITEM_NOT_ALLOWED = "itemNotAllowed";
	public static final String PAYMENTSYSTEMS_PROD_CODE = "paymentSystemsProductCode";
	public static final String PAYMENTSYSTEM_PROD_CODE = "paymentSystemProductCode";
	public static final String PAYMENTS = "payments";
	public static final String PAYMENT = "payment";
	public static final String CHANGE_AMOUNT= "changeAmount";
	public static final String ELECTRONIC = "electronic";
	public static final String EPS_DATA = "epsData";
	public static final String PRINT_RECEIPT = "printReceipt";
	public static final String TOTAL = "total";
	public static final String MOBILE_PAYMENT_DATA = "mobilePaymentData";
	public static final String USER_INTERFACE = "userInterface";
	public static final String FIXED_DISC = "fixedDisc";
	public static final String MANUAL_DISC = "manualDisc";
	public static final String UNIT_DISC= "unitDiscount";
	public static final String LABEL="label";
	public static final String PROMO_DISC = "pluPromo";
	public static final String NAXML_DISC = "naxmlDisc";
	public static final String SCREEN_CONFIG = "screenConfig";
	public static final String DISPLAY_ICON = "displayIcon";
	public static final String LOYALTYDISC="loyaltyDisc";

	//EPSData
	public static final String DEVICE_REQUEST = "DeviceRequest";
	public static final String OUTPUT = "Output";
	public static final String TEXTLINE = "TextLine";
	public static final String ALIGNMENT = "Alignment";
	public static final String HEIGHT = "Height";
	public static final String WIDTH = "Width";
	public static final String CHARSTYLE1 = "CharStyle1";
	public static final String CHARSTYLE2 = "CharStyle2";
	public static final String CHARSTYLE3 = "CharStyle3";
	public static final String PAPER_CUT = "PaperCut";
	public static final String OUT_DEVICE_TARGET = "OutDeviceTarget";
	public static final String COPIES = "Copies";
	public static final String RECEIPT_RECIPIENT = "ReceiptRecipient";
	public static final String CARD_SERVICE_RESPONSE = "CardServiceResponse";
	public static final String REQUEST_ID = "RequestID";
	public static final String REQUEST_TYPE = "RequestType";
	public static final String TENDER = "Tender";
	public static final String TERMINAL = "Terminal";
	public static final String TERMINAL_BATCH = "TerminalBatch";
	public static final String TERMINALID = "TerminalID";
	public static final String STAN = "STAN";
	public static final String TOTAL_AMOUNT = "TotalAmount";
	public static final String AUTHORIZATION = "Authorization";
	public static final String CARD_PRE_AUTHORIZATION = "CardPreAuthorization";
	public static final String TIME_STAMP = "TimeStamp";
	public static final String APPROVAL_CODE = "ApprovalCode";
	public static final String CARD_CIRCUIT = "CardCircuit";
	public static final String SIGNATURE_ON_FINANCIAL_ADVICE= "SignatureOnFinancialAdvice";
	public static final String CARD_ENTRY_MODE = "CardEntryMode";
	public static final String USED_PAYMENT_METHOD = "UsedPaymentMethod";
	public static final String CREDIT = "Credit";
	public static final String DEBIT = "Debit";
	public static final String MANUAL = "Manual";
	public static final String PREPAID = "Prepaid";

	//Mobile Payment Data
	public static final String PAYMENT_INFO = "paymentInfo";
	public static final String AUTH_REFERENCE = "authReference";
	public static final String SECURE_TOKEN = "secureToken";
	public static final String AMOUNT = "amount";
	public static final String MOP_CODE = "mopCode";
	public static final String CARD_ENTRY_MODE_TYPE = "cardEntryMode";
	public static final String PAYMENT_METHOD = "paymentMethod";
	public static final String REFERENCE_NUMBER = "referenceNumber";
	public static final String CARD_TYPE_LABEL = "cardType";
	public static final String AUTH_REFERENCE_ID = "authReferenceID";
	public static final String AUTH_DATETIME = "authDateTime";
	public static final String SEQUENCE_NUMBER = "sequenceNumber";
	public static final String RECEIPT_NETWORK_TEXT = "receiptNetworkText";
	public static final String CHAR_STYLE = "charStyle";
	public static final String TEXT = "text";
	public static final String MOP = "mop";
	public static final String SYSID = "sysid";
	public static final String IS_CARD_BASED = "isCardBased";
	public static final String NAME = "name";

	//salesProcessorRtn
	public static final String SALES_PROCESSOR_RTN = "salesProcessorRtn";
	public static final String BEGIN_SESSION_RESP = "beginSessionResp";
	public static final String EVALUATE_SALE_RESP = "evaluateSaleResp";
	public static final String REFRESH_CONFIG_RESP = "refreshConfigurationResp";
	public static final String FINALIZE_SALE_RESP = "finalizeSaleResp";
	public static final String END_SESSION_RESP = "endSessionResp";
	public static final String EVALUATED_EXTERNAL_SALE = "evaluatedExternalSale";
	public static final String POSTED_EXTERNAL_SALE = "postedExternalSale";
	public static final String RESULT = "result";
	public static final String BEGIN_SESSION = "beginSession";
	public static final String USER_NAME = "userName";
	public static final String SECURITY_LEVEL = "securityLevel";
	public static final String REFRESH_CONFIG = "refreshConfiguration";
	public static final String EVAL_SALE = "evaluateSale";
	public static final String POST_FINANCIAL_TXN = "postFinancialTxn";
	public static final String UNPOST_FINANCIAL_TXN = "unpostFinancialTxn";
	public static final String GENERATE_RECEIPT = "generateReceipt";
	public static final String POST_FINANCIAL_TXN_RESP = "postFinancialTxnResp";
	public static final String UNPOST_FINANCIAL_TXN_RESP = "unpostFinancialTxnResp";
	public static final String GENERATE_RECEIPT_RESP = "generateReceiptResp";
	public static final String FINALIZE_SALE = "finalizeSale";
	public static final String END_SESSION = "endSession";
	public static final String SUSPEND_TXN_RESP = "suspendFinancialTxnResp";
	public static final String RECALL_TXN_RESP = "recallFinancialTxnResp";
	public static final String EXT_CONFIG = "externalConfiguration";
	public static final String EXT_SELLABLE_ITEMS = "sellableItems";
	public static final String PLU = "plu";
	public static final String BALANCE_DUE="balanceDue";
	public static final String DEPT = "dept";
	public static final String TRIGGER_POSTFINANCIAL_TXN ="triggerPostFinancialTxn";
	public static final String GENERATE_STICKY_LABEL_RESP = "generateStickyLabelsResp";

	// Carwash details
	public static final String CARWASH_CODE_ENTITY = "carwashCode";
	public static final String CARWASH_CODE = "washCode";
	public static String CARWASH_CODE_VALID_DAYS = "validDays";

	public static final String PRICE_LEVEL = "priceLevel";
	public static final String  APT_CFG = "aptConfig";

	//Generate receipt
	public static final String FORMATTED_RECEIPT_TEXT = "formattedReceiptText";
	public static final String HEADER = "header";
	public static final String BODY = "body";
	public static final String FOOTER = "footer";
	public static final String COLUMN = "column";
	public static final String CHAR_SET = "charSet";
	public static final String ECHO = "echo";
	public static final String CURSOR = "cursor";
	public static final String COLOR = "color";
	public static final String CHAR_STYLE1 = "charStyle1";
	public static final String BAR_CODE_FORMAT = "barCodeFormat";
	public static final String PAPERCUT = "paperCut";
	public static final String SIGNATURE_REF = "signatureRef";
	public static final String RECEIPT_TEXT_REQD = "receiptTextReqd";
	public static final String ALIGNMENT_SMALL_CASE = "alignment";
	public static final String HEIGHT_SMALL_CASE = "height";
	public static final String WIDTH_SMALL_CASE = "width";
	public static final String RIGHT = "Right";
	public static final String CENTER = "Center";
	public static final String LEFT = "Left";
	public static final String SINGLE = "Single";
	public static final String DOUBLE = "Double";
	public static final String HALF = "Half";
	public static final String NORMAL = "Normal";
	public static final String BOLD = "Bold";
	public static final String ITALIC = "Italic";
	public static final String UNDERLINED = "Underlined";
	public static final String STICKY_LABEL = "stickyLabel";

	//tillProcessorCmd
	public static final String TILL_PROCESSOR_RTN = "tillProcessorRtn";
	public static final String SEQ_NUMBER ="sequenceNumber";
	public static final String OPEN_DATE ="openDate";
	public static final String CLOSE_DATE ="closeDate";
	public static final String OPEN_CASHIER = "openCashier";
	public static final String CLOSE_CASHIER = "closeCashier";
	public static final String OPEN_CASHIER_RESP = "openCashierResp";
	public static final String CLOSE_CASHIER_RESP = "closeCashierResp";
	public static final String BEGIN_CASH = "beginCashAmount";
	public static final String SAFE_DROP = "safeDrop";
	public static final String SAFE_LOAN = "safeLoan";
	public static final String SAFEDROP_RESP = "safeDropResp";
	public static final String SAFELOAN_RESP = "safeLoanResp";
	public static final String DRAWER="drawer";
	public static final String NUMBER="number";


	// Refresh Config Response
	public static final String POST_FINANCIALTXN_FREQUENCY = "postFinancialTxnFrequency";

	// result types
	public static final String SUCCESS="success";
	public static final String FAILURE="failure";
	public static final String WEBSOCKET_TAKEOVER="websocket_takeover";


	public static CharSequence FuelSaleTrx = "<FuelSaleTrx";
	public static CharSequence FuelSaleTrxEnd = "</FuelSaleTrx>";

	public static String payable = "Payable";
	public static String locked = "Locked";

}
