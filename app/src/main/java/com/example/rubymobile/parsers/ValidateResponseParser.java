package com.example.rubymobile.parsers;


import com.example.rubymobile.ent.TerminalDataSet;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ValidateResponseParser extends DefaultHandler {
	public ValidateResponseParser() {

	}
	String cookie;
	StringBuffer buffer = new StringBuffer();
	 @Override
	   public void startElement(String uri, String localName, String qName, Attributes attributes)
			   throws SAXException {
		 buffer.delete(0, buffer.length());
	 }
	 @Override
	 public void endElement(String uri, String localName, String qName)
			   throws SAXException{
		 if(localName.equalsIgnoreCase("cookie")) {
			 cookie = buffer.toString();
			 TerminalDataSet.setCookie(cookie);
		 }
	 }
	 
	  @Override
	   public void characters(char ch[], int start, int length) throws SAXException {
		   buffer.append(ch, start, length);
	   }
}
