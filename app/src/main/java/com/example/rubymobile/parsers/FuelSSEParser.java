package com.example.rubymobile.parsers;

import com.example.rubymobile.ent.DeviceClass;
import com.example.rubymobile.ent.FPStatusEvent;
import com.example.rubymobile.ent.IFuelEvent;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

import static com.example.rubymobile.parsers.VistConstants.DeviceClass;

public class FuelSSEParser extends DefaultHandler {

    public FuelSSEParser() {
    }

    public IFuelEvent getEvent() {
        return fpEvent;
    }

    List<IFuelEvent> events = new ArrayList<>();
    boolean isFuelStateChange;
    FPStatusEvent fpEvent            = new FPStatusEvent();

    com.example.rubymobile.ent.DeviceClass deviceClass = new DeviceClass();;
    StringBuffer buffer = new StringBuffer();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {
        buffer.delete(0, buffer.length());

        if (localName.equalsIgnoreCase(VistConstants.FPStateChange)) {
            isFuelStateChange = true;
            deviceClass = new DeviceClass();

        }
        if (localName.equalsIgnoreCase(DeviceClass)) {
            String deviceID = attributes.getValue("DeviceID");
			deviceClass.setDeviceID(deviceID);
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        if (localName.equalsIgnoreCase(VistConstants.DeviceState)) {
			deviceClass.setDeviceState(buffer.toString());
			fpEvent.getFpStatus().add(deviceClass);
			deviceClass = new DeviceClass();
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        buffer.append(ch, start, length);
    }
}
