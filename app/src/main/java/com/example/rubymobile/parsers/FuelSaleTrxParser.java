package com.example.rubymobile.parsers;

 import com.example.rubymobile.ent.DeviceClassData;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

public class FuelSaleTrxParser extends DefaultHandler  {


	private StringBuffer buffer = new StringBuffer();
	private String devId;
	private DeviceClassData data;

	public ArrayList<DeviceClassData> getDeviceClasses() {
		return deviceClasses;
	}

	private ArrayList<DeviceClassData> deviceClasses = new ArrayList();

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException {
		buffer.delete(0, buffer.length());
		if(localName.equalsIgnoreCase("FDCdata")) {

		}
		else if(localName.equalsIgnoreCase("DeviceClass")) {
			if(data != null && data.getDeviceID().equals("ID")) {
				devId = attributes.getValue("DeviceID");
				data = new DeviceClassData();
				data.setDeviceID(devId);
			}else if(data == null ) {
				devId = attributes.getValue("DeviceID");
				data = new DeviceClassData();
				data.setDeviceID(devId);
			}
 		}
	}
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException{
		if(localName.equalsIgnoreCase(VistConstants.AMOUNT)) {
			if(data.getAmount1()== null) {
				data.setAmount1(buffer.toString());
			}else {
				data.setAmount2(buffer.toString());
			}
		}else if(localName.equalsIgnoreCase("State")){
			if(data.getAmount1()== null) {
				data.setState1(buffer.toString());
			}else {
				data.setState2(buffer.toString());
			}
		}
		else if(localName.equalsIgnoreCase("DeviceClass")) {
			if(data.getDeviceID()!= null &&
					data.getAmount1() != null && data.getAmount2() != null) {
				deviceClasses.add(data)	;
				data = null;
			} 
 		}
		
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		buffer.append(ch, start, length);
	}
}
