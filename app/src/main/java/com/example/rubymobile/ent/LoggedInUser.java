package com.example.rubymobile.ent;

import androidx.lifecycle.ViewModel;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class LoggedInUser extends ViewModel {

    private String userId;
    private String password;

    public LoggedInUser(String userId, String password) {
        this.userId = userId;
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public String getPassword() {
        return password;
    }
}
