package com.example.rubymobile.ent;

/**
 * 
 * @author T_VaishnaviS1
 *
 */
public class ReceiptText {
	
	private String text;
	private String alignment;
	
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getAlignment() {
		return alignment;
	}
	public void setAlignment(String alignment) {
		this.alignment = alignment;
	}

}
