package com.example.rubymobile.ent;

import java.util.ArrayList;

public class PostedTransaction {

	boolean isSaleCompleted;
	String evaluatedTotalTaxAmt;
	String evaluatedTotal;
	private String balanceDue;
	private String transNum;


	ArrayList<ReceiptText> header;

	ArrayList<ReceiptText> body;

	ArrayList<ReceiptText> footer;

	private boolean isResSuccess;
	private String failurereason;

	public ArrayList<ReceiptText> getHeader() {
		return header;
	}

	public void setHeader(ArrayList<ReceiptText> header) {
		this.header = header;
	}

	public ArrayList<ReceiptText> getBody() {
		return body;
	}

	public void setBody(ArrayList<ReceiptText> body) {
		this.body = body;
	}

	public ArrayList<ReceiptText> getFooter() {
		return footer;
	}

	public void setFooter(ArrayList<ReceiptText> footer) {
		this.footer = footer;
	}

	public String getFailurereason() {
		return failurereason;
	}
	public void setFailurereason(String failurereason) {
		this.failurereason = failurereason;
	}
	public boolean isResSuccess() {
		return isResSuccess;
	}
	public void setResSuccess(boolean isResSuccess) {
		this.isResSuccess = isResSuccess;
	}
	public boolean isSaleCompleted() {
		return isSaleCompleted;
	}
	public void setSaleCompleted(boolean isSaleCompleted) {
		this.isSaleCompleted = isSaleCompleted;
	}
	public String getEvaluatedTotalTaxAmt() {
		return evaluatedTotalTaxAmt;
	}
	public void setEvaluatedTotalTaxAmt(String evaluatedTotalTaxAmt) {
		this.evaluatedTotalTaxAmt = evaluatedTotalTaxAmt;
	}
	public String getEvaluatedTotal() {
		return evaluatedTotal;
	}
	public void setEvaluatedTotal(String evaluatedTotal) {
		this.evaluatedTotal = evaluatedTotal;
	}
	public void setBalanceDue(String balanceDue){
		this.balanceDue = balanceDue;
	}
	public String getBalanceDue(){
		return this.balanceDue;
	}

	public void setTransNum(String transNum){
		this.transNum = transNum;
	}

	public String getTransNum(){
		return this.transNum;
	}
	PostPay postpayFuel;
    public void setPostPayItem(PostPay postpayFuel) {
    	this.postpayFuel = postpayFuel;
    }

	public PostPay getPostPayItem( ) {
		return this.postpayFuel;
	}
}

