package com.example.rubymobile.ent;



public class TerminalDataSet {
    static  String terminalid;
    static String cookie;
    static String appType;
    static String extSessionID;static String pwd;
    static LoggedInUser user;
    public static LoggedInUser getUser () {
        return user;
    }

    public static void setUser (LoggedInUser user ) {
        TerminalDataSet.user = user;
    }

    public static String getextSessionID() {
        return extSessionID;
    }

    public static void setextSessionID(String pwd) {
        TerminalDataSet.extSessionID = pwd;
    }

    public static String getTerminalid() {
        return terminalid;
    }

    public static void setTerminalid(String terminal) {
         terminalid = terminal;
    }

    public static String getCookie() {
        return cookie;
    }

    public static String getAppType() {
        return appType;
    }

    public static void setAppType(String appType) {
        TerminalDataSet.appType = appType;
    }

    public static void setCookie(String cooki) {
          cookie = cooki;
    }


}
