package com.example.rubymobile.ent;

public class DeviceClassData implements IFuelEvent {

	String DeviceID = "ID";
	String amount1;
	String amount2;
	String state1;

	public String getState2() {
		return state2;
	}

	public void setState2(String state2) {
		this.state2 = state2;
	}

	String state2;
	public String getState1() {
		return state1;
	}

	public void setState1(String state) {
		this.state1 = state;
	}

	public String getDeviceID() {
		return DeviceID;
	}
	public void setDeviceID(String deviceID) {
		DeviceID = deviceID;
	}
	public String getAmount1() {
		return amount1;
	}
	public void setAmount1(String amount1) {
		this.amount1 = amount1;
	}
	public String getAmount2() {
		return amount2;
	}
	public void setAmount2(String amount2) {
		this.amount2 = amount2;
	}
}
