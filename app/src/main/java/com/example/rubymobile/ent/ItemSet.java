package com.example.rubymobile.ent;

import java.util.ArrayList;
import java.util.List;

public class ItemSet {
	private String name;
	private String iconName;
	private String colorCode;
	private List<ItemSubset> itemSubsets = new ArrayList<ItemSubset>();
	private String iconUrl;
	
	private String dateModified;


	public static final int ITEMSET_NAME_LENGTH = 12; 
	public static final int MAX_ITEMSET_ID = 99;
	public static final int MAX_COLOR_CODE = 7;
	public static final String [] COLOR_CODES = new String[] {"#2D8F63", "#607FB2", "#AA3939", "#AA5C39", "#AA7539", "#865131", "#666666"};

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIconName() {
		return iconName;
	}

	public String getDateModified() {
		return dateModified;
	}
	public void setDateModified(String dateModified) {
		this.dateModified = dateModified;
	}
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getColorCode() {
		return colorCode;
	}
	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}
	public List<ItemSubset> getItemSubsets() {
		return itemSubsets;
	}
	public void setItemSubsets(List<ItemSubset> itemSubsets) {
		this.itemSubsets = itemSubsets;
	}
	public static String[] getColorCodes() {
		return COLOR_CODES;
	}
		
}

