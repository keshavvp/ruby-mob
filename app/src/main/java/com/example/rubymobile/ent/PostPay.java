package com.example.rubymobile.ent;

public class PostPay implements ItemLine {

    public String getPositionNum() {
        return positionNum;
    }

    public void setPositionNum(String positionNum) {
        this.positionNum = positionNum;
    }

    String DisplayName;
    String positionNum;
    String faceVolume;
    String service;

    String amount;

    @Override
    public String getDisplayName() {
        return DisplayName;
    }

    @Override
    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    @Override
    public String getDateModified() {
        return null;
    }

    @Override
    public void setDateModified(String dateModified) {

    }

    @Override
    public String getIconName() {
        return null;
    }

    @Override
    public void setIconName(String iconName) {

    }

    @Override
    public String getIconUrl() {
        return null;
    }

    @Override
    public void setIconUrl(String iconUrl) {

    }

    @Override
    public boolean isHotItem() {
        return false;
    }

    @Override
    public void setHotItem(boolean hotItem) {

    }

    @Override
    public int getQty() {
        return 0;
    }

    @Override
    public void setQty(int qty) {

    }

    @Override
    public String getUPC() {
        return null;
    }

    @Override
    public void setUPC(String uPC) {

    }

    @Override
    public String getModifier() {
        return null;
    }

    @Override
    public void setModifier(String modifier) {

    }

    @Override
    public String getAmount() {
        return amount;
    }

    @Override
    public void setAmount(String amount) {
        this.amount=amount;

    }

    @Override
    public String getSym() {
        return null;
    }

    @Override
    public void setSym(String sym) {

    }


    public void faceVolume(String toString) {
        faceVolume = toString;
    }

    public String getfaceVolume() {
       return  faceVolume  ;
    }
    public String getservicelevel() {
        return  service  ;
    }

    public void setServiceLevel(String toString) {
        service = toString;
    }
}
