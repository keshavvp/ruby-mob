package com.example.rubymobile.handler;


import com.example.rubymobile.com.example.adatper.CustomAddedItemDataAdapter;
import com.example.rubymobile.controller.PromptController;
import com.example.rubymobile.data.TransactionViewModelFactory;
import com.example.rubymobile.ent.PostedTransaction;

public class PostFinTxnResponseHandler {

   static CustomAddedItemDataAdapter updaterAdapter;

    PostedTransaction txn;
    public PostFinTxnResponseHandler(PostedTransaction txn) {
        this.txn =txn;
    }
    public static void setUpdaterAdapter(CustomAddedItemDataAdapter ad){
        updaterAdapter = ad;
    }
    public void handle() throws Exception{
        if(!txn.isResSuccess()){
            updaterAdapter.notifySaleResponse(false,txn,txn.getFailurereason());

            throw new Exception("Sale Evaluation Failed : "+ txn.getFailurereason());
        }else if(txn.isResSuccess()){
            if (txn.isSaleCompleted()){
                TransactionViewModelFactory.getTransaction().clear();
                updaterAdapter.notifySaleResponse(txn.isResSuccess(),txn,"sale completed");
             }else {
                TransactionViewModelFactory.getTransaction().setTax(txn.getEvaluatedTotalTaxAmt());
                TransactionViewModelFactory.getTransaction().setTotal(txn.getEvaluatedTotal());
                updaterAdapter.notifySaleResponse(false,txn,"");
            }
        }
    }
}
