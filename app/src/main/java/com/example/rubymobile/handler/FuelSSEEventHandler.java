package com.example.rubymobile.handler;

import com.example.rubymobile.com.example.adatper.FuelIconAdapter;
import com.example.rubymobile.ent.DeviceClassData;
import com.example.rubymobile.ent.FPStatusEvent;
import com.example.rubymobile.ent.IFuelEvent;

import java.util.ArrayList;

public class FuelSSEEventHandler {
    private static FuelIconAdapter fuelFragment;
    IFuelEvent event;
    ArrayList<DeviceClassData> deviceClasses;

    public FuelSSEEventHandler(IFuelEvent event) {
        this.event = event;
    }

    public FuelSSEEventHandler(ArrayList<DeviceClassData> deviceClasses) {
        this.deviceClasses = deviceClasses;
    }

    public static void setObserver(FuelIconAdapter fuelFrag) {
        fuelFragment = fuelFrag;
    }

    public void handle() throws Exception {
        if (event != null && event instanceof FPStatusEvent) {
            System.out.println("Handling SSE");

            fuelFragment.notifySSEEvent(event);
        } else {
            fuelFragment.notifyFuelSaleTrx(deviceClasses);
        }
    }
}
