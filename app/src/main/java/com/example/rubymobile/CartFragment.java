package com.example.rubymobile;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.rubymobile.com.example.adatper.CustomAddedItemDataAdapter;
import com.example.rubymobile.data.CommandType;
import com.example.rubymobile.data.ConfigDataViewModelFactory;
import com.example.rubymobile.data.RefreshConfigData;
import com.example.rubymobile.data.TransactionViewModelFactory;
import com.example.rubymobile.ent.Item;
import com.example.rubymobile.ent.ItemLine;
import com.example.rubymobile.ent.Payment;
import com.example.rubymobile.ent.TerminalDataSet;
import com.example.rubymobile.network.HttpConnectionUtil;
import com.example.rubymobile.payment.PaymentGridAdapter;
import com.example.rubymobile.pres.IBackPressed;
import com.example.rubymobile.write.Xmlwriter;
import com.google.android.material.snackbar.Snackbar;

import java.math.BigDecimal;
import java.util.ArrayList;


public class CartFragment extends Fragment implements IBackPressed {
    static CartFragment myInstance;
    ListView list;
    View cartView;
    View mopView;
    GridView paymentGrid;
    ProgressBar bar;
    TextView progressBartxt;
    RefreshConfigData dataModel;
    CustomAddedItemDataAdapter cartAdapter;

    public static CartFragment getInstance() {
        if (myInstance == null) {
            myInstance = new CartFragment();
        }
        return myInstance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout l = new LinearLayout(getContext());
        cartView = inflater.inflate(R.layout.fragment_cart, container, false);
        mopView = inflater.inflate(R.layout.moplist_view, container, false);
        mopView.setVisibility(View.GONE);
        l.addView(cartView);
        l.addView(mopView);

        list = cartView.findViewById(R.id.receiptList);
        Button suspend = cartView.findViewById(R.id.suspend);
        suspend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HttpConnectionUtil con = new HttpConnectionUtil();
                Runnable run = () -> {
                    try {
                        con.sendExtposReq(CommandType.SUSP_TXN, TerminalDataSet.getCookie(), Xmlwriter.getSuspendCashierXmlPayload());
                    } catch (Exception e) {
                        Log.d("suspend", "error while suspending ssale");
                    }
                };
                new Thread(run).start();
                showSnakBar("Sale is getting Suspend");

            }
        });
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Button btn = view.findViewById(R.id.delete);
                if (btn.getVisibility() == View.VISIBLE) {
                    btn.setVisibility(View.GONE);


                }
            }
        });
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                Button btn = view.findViewById(R.id.delete);
                btn.setVisibility(View.VISIBLE);
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ArrayList<ItemLine> items = TransactionViewModelFactory.getTransaction().getItems();
                        items.remove(position);
                        cartAdapter.notifyDataSetChanged();

                    }
                });
                return true;
            }
        });
        Button voidBtn = cartView.findViewById(R.id.voidBtn);
        voidBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HttpConnectionUtil con = new HttpConnectionUtil();
                Runnable run = () -> {
                    try {
                        con.sendExtposReq(CommandType.VOID, TerminalDataSet.getCookie(), Xmlwriter.getVoidPayload());
                    } catch (Exception e) {
                        Log.d("suspend", "error while voiding sale");
                    }
                };
                new Thread(run).start();
                showSnakBar("Sale is getting Voided");
            }
        });


        TextView totalPrice = cartView.findViewById(R.id.totalPrice);
        Button payBtn = cartView.findViewById(R.id.pay);
        payBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mopView.setVisibility(View.VISIBLE);
                cartView.setVisibility(View.GONE);
                //        getSupportActionBar().hide();
                paymentGrid = mopView.findViewById(R.id.payGrid);
                bar = mopView.findViewById(R.id.payProgressBar);
                progressBartxt = mopView.findViewById(R.id.progress_text);
                progressBartxt.setVisibility(View.GONE);
                bar.setVisibility(View.GONE);
//        PromptController.getMyInstance(this);
//        this.setFinishOnTouchOutside(false);


                paymentGrid.setAdapter(new PaymentGridAdapter(paymentGrid, getActivity()));
                paymentGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Payment selectedPayment = (Payment) paymentGrid.getAdapter().getItem(position);
                        TransactionViewModelFactory.getTransaction().getPayment().add(selectedPayment);
                        paymentGrid.setEnabled(false);
                        HttpConnectionUtil util = new HttpConnectionUtil();
                        Runnable run = () -> {
                            try {
                                util.sendExtposReq(CommandType.POST_SALE, TerminalDataSet.getCookie(), Xmlwriter.getPostSaleReqPayload());
                            } catch (Exception e) {
                            }
                        };
                        new Thread(run).start();

                        bar.setVisibility(View.VISIBLE);
                        progressBartxt.setVisibility(View.VISIBLE);
                    }
                });

            }
        });
        dataModel = ViewModelProviders.of(this, new ConfigDataViewModelFactory(getActivity().getApplication()))
                .get(RefreshConfigData.class);


        Item it = new Item();
        it.setAmount(BigDecimal.TEN);


        return l;
    }

    public void showSnakBar(String messages) {

        Snackbar.make(getActivity().getWindow().getDecorView(), messages, Snackbar.LENGTH_LONG)
                .setActionTextColor(Color.WHITE)
                .show();

    }

    @Override
    public void onStart() {
        super.onStart();
        cartAdapter = new CustomAddedItemDataAdapter(getActivity(), dataModel.getConfiguration(), list, cartView);
        list.setAdapter(cartAdapter);
    }
    public void enablePAyGird(boolean flag){
        if(paymentGrid != null && bar != null) {
            if(flag){
                paymentGrid.setEnabled(true);
                bar.setVisibility(View.GONE);
                progressBartxt.setVisibility(View.GONE);
                mopView.setVisibility(View.GONE);
                cartView.setVisibility(View.VISIBLE);
            }else{
                paymentGrid.setEnabled(false);
                progressBartxt.setVisibility(View.VISIBLE);
                bar.setVisibility(View.VISIBLE);
            }

        }
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean backPressed() {
        boolean callSuper = true;
        if (mopView != null && View.VISIBLE == mopView.getVisibility()) {
            cartView.setVisibility(View.VISIBLE);
            mopView.setVisibility(View.INVISIBLE);
            paymentGrid.setEnabled(true);
            callSuper = false;
        }
        return callSuper;
    }
}