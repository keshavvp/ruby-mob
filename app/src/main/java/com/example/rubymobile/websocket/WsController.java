package com.example.rubymobile.websocket;

import android.util.Log;

 import com.example.rubymobile.ent.TerminalDataSet;

import java.net.URI;

import javax.net.ssl.HttpsURLConnection;

public class WsController {
    WsClient chatclient;

      public void connect() throws Exception {
//            WsClient chatclient = new WsClient( new URI( "wss://10.0.2.2:8887" ) );

          String EXTSERVER = "/ExtActionWSServer/extposAction/";

            String protocol = "wss";
            String url = protocol + "://" + "192.168.31.11" +":" + "8443" +
            EXTSERVER + TerminalDataSet.getTerminalid() + "?" + TerminalDataSet.getextSessionID();
            chatclient = new WsClient( new URI( url ) );

            chatclient.setSocketFactory(HttpsURLConnection.getDefaultSSLSocketFactory());
            chatclient.connectBlocking();
            Log.d("Websocket","connection status :" +chatclient.isOpen());

            // sslContext.init( null, null, null ); // will use java's default key and trust store which is sufficient unless you deal with self-signed certificates




            System.out.println("yes!");


        }
        public boolean isWsConnected(){
          return  chatclient.isOpen();
        }
    }
