package com.example.rubymobile;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rubymobile.data.CommandType;
import com.example.rubymobile.data.HELPER.FileHelper;
import com.example.rubymobile.ent.LoggedInUser;
import com.example.rubymobile.ent.TerminalDataSet;
import com.example.rubymobile.ent.TokenHolder;
import com.example.rubymobile.network.HttpConnectionUtil;
import com.example.rubymobile.parsers.TokenParser;
import com.example.rubymobile.write.Xmlwriter;
import com.google.android.material.snackbar.Snackbar;

import org.xml.sax.InputSource;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import okhttp3.OkHttpClient;

import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

public class LoginActivity extends AppCompatActivity {

    SSLContext context;
    SharedPreferences sharedPref;
    AlertDialog dialog;
    LoginActivity act;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_login);
        hideSystemViews();
        createTLSBackgroud(); // bundle and start tlss configs
        sharedPref = this.getPreferences(Context.MODE_PRIVATE);//start sp
        final EditText usernameEditText = findViewById(R.id.userId);
        final EditText passwordEditText = findViewById(R.id.password);
        ImageButton imageSetting =  findViewById(R.id.setting);
        imageSetting.setBackground(getDrawable(R.drawable.setting1));
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);
        act = this;

        imageSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"This is settings",Toast.LENGTH_SHORT).show();
            }
        });
      Button signIn =  findViewById(R.id.signIn);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)  /*{
                Intent in = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(in);
                finish();
            }*/  {
                loadingProgressBar.setVisibility(View.VISIBLE);
                TerminalDataSet.setUser (new LoggedInUser(usernameEditText.getText().toString(),passwordEditText.getText().toString()));
                HttpConnectionUtil con = new HttpConnectionUtil();
                FileHelper.setContext(getApplicationContext());
                Runnable sendValidate = ()->{
                    try{
                        String token   = sharedPref.getString("token","6ed8f0cde219992bff60be4ee3b1c41c64ff7c39");
                        String terminalID   = sharedPref.getString("terminalID","169");
                        String appType   = sharedPref.getString("appType","CarbonSelfCheckout");

                      /*   String token   = sharedPref.getString("token",null);
                        String terminalID   = sharedPref.getString("terminalID",null);
                        String appType   = sharedPref.getString("appType",null);*/
                        TerminalDataSet.setTerminalid(terminalID);
                        TerminalDataSet.setAppType(appType);
                        TokenHolder.setToken(token);
                       con.sendValidateCommand("validate",terminalID, token);
                        con.sendExtposReq(CommandType.BEGIN_SESSION,TerminalDataSet.getCookie(), Xmlwriter.getBeginSessionXml());

                        Intent in = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(in);
                        finish();
                    }catch(Exception e) {
                        showSnakBar(v,loadingProgressBar, e.getMessage());
                        //                   Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                    }

                };
                new Thread(sendValidate).start();

            }
        });

        ImageView utility =  findViewById(R.id.setting);
        utility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)  {
                final AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                View inputMenu = getLayoutInflater().inflate(R.layout.custom_input_menu_items, null);
                TextView header = inputMenu.findViewById(R.id.header);
                header.setText("Settings");
                String regID   = sharedPref.getString("terminalID","168");
                String portNum   = sharedPref.getString("port","9000");
                TextView labelTerminalId =  inputMenu.findViewById(R.id.label1);
                labelTerminalId.setText("Terminal ID");
                TextView terminalID=  inputMenu.findViewById(R.id.inputText1);
                terminalID.setText(regID);
                TextView labelPort =  inputMenu.findViewById(R.id.label2);
                labelPort.setText("Port");
                TextView port =  inputMenu.findViewById(R.id.inputText2);
                port.setInputType(InputType.TYPE_CLASS_NUMBER );
                port.setText(portNum);
                port.setVisibility(View.VISIBLE);
                TextView amT =  inputMenu.findViewById(R.id.inputText2);
                amT.setVisibility(View.VISIBLE);
                Button okBtn =  inputMenu.findViewById(R.id.ok);
                Button cancel =  inputMenu.findViewById(R.id.cancel);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                okBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences.Editor editor = sharedPref.edit();

                        editor.putString("terminalID", terminalID.getText().toString());
                        editor.putString("port", port.getText().toString());
                        editor.commit();
                        moveToRegisterModeSetting();

                    }
                });

                alert.setView(inputMenu);
                dialog = alert.create();dialog.show();
            }
        });

    }

    public static final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.KEEP_SCREEN_ON
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

    public void hideSystemViews() {
        View decorView = getWindow().getDecorView();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        decorView.setSystemUiVisibility(flags);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        hideUtil();


    }

    public void hideUtil() {
        getWindow().getDecorView().getRootView().setSystemUiVisibility(flags);

        // Code below is to handle presses of Volume up or Volume down.
        // Without this, after pressing volume buttons, the navigation bar will
        // show up and won't hide
        final View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(flags);
        decorView.getRootView()
                .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {

                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        getWindow().getDecorView().getRootView().setSystemUiVisibility(flags);
                        getWindow().getDecorView().setSystemUiVisibility(flags);

                        decorView.setSystemUiVisibility(flags);
                    }
                });
    }

    TrustManager[] trustAllCertificates = new TrustManager[]{
            new X509TrustManager() {

                @Override
                public void checkClientTrusted(X509Certificate[] arg0,
                                               String arg1) throws CertificateException {
                    // TODO Auto-generated method stub
                    System.out.println(arg0);
                }

                @Override
                public void checkServerTrusted(X509Certificate[] arg0,
                                               String arg1) throws CertificateException {
                    // TODO Auto-generated method stub
                    System.out.println(arg0[0].getSubjectDN().getName());
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    // TODO Auto-generated method stub
                    return null;
                }

            }

    };

    HostnameVerifier trustAllHostnames = new HostnameVerifier() {

        @Override
        public boolean verify(String arg0, SSLSession arg1) {
            // TODO Auto-generated method stub
            return true;
        }
    };
    public void   showSnakBar(View v,ProgressBar p ,String messages){
        Runnable serveUI =() ->{  act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                p.setVisibility(View.GONE);
                Snackbar.make(v, messages, Snackbar.LENGTH_SHORT).show();

            }
        });};
        new Thread(serveUI).start();
    }
    OkHttpClient okHttpClient_client;
    private void createTLSBackgroud() {
        try{
            KeyManagerFactory kmf;
            KeyStore ks;
            char[] passphrase = "verifone".toCharArray();
            context = SSLContext.getInstance("TLS");
            SSLContext.setDefault(context);
            kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());//x509
            ks = KeyStore.getInstance("BKS");

            InputStream in = getBaseContext().getResources().openRawResource(R.raw.mytruststore);
            ks.load( in , passphrase);
            kmf.init(ks, passphrase);
            TrustManagerFactory tmf = TrustManagerFactory.getInstance( "PKIX" );
            tmf.init( ks );
            context.init(kmf.getKeyManagers(), trustAllCertificates, null);

            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(trustAllHostnames);

            okHttpClient_client =
                    new OkHttpClient.Builder().
                            sslSocketFactory(context.getSocketFactory(),
                                    x
                            ).hostnameVerifier(trustAllHostnames).build();
            HttpConnectionUtil.setOkCLient(okHttpClient_client);
        }catch (Exception e){

        }
    }
    X509TrustManager x = new X509TrustManager() {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0,
                                       String arg1) throws CertificateException {
            // TODO Auto-generated method stub
            System.out.println(arg0);
        }
        X509Certificate cet ;
        @Override
        public void checkServerTrusted(X509Certificate[] arg0,
                                       String arg1) throws CertificateException {
            // TODO Auto-generated method stub
            cet = arg0[0];
            System.out.println(arg0[0].getSubjectDN().getName());
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            // TODO Auto-generated method stub
            return  new X509Certificate[0] ;
        }

    };
    private void moveToRegisterModeSetting() {
        String regID   = sharedPref.getString("terminalID","168");
        String portNum   = sharedPref.getString("port","9000");
        SharedPreferences.Editor editor = sharedPref.edit();
        final ProgressDialog pd = new ProgressDialog(LoginActivity.this);
        pd.setMessage("Registration in Progress");
        pd.show();
        SSLSocket cSocket;
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{


                    SSLServerSocketFactory factory;


                    factory = context.getServerSocketFactory();
                    SSLServerSocket serverSocket =
                            (SSLServerSocket) factory.createServerSocket(Integer.parseInt(portNum), 0);
                    serverSocket.setEnabledProtocols(new String[] {"TLSv1.1","TLSv1.2"});
                    Log.v("Registration :", "waiting for token " + portNum);
                    SSLSocket cSocket = null;
                    BufferedInputStream bis;
                    String xmldata;
                    String res= "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><domain:registerDeviceResponse xmlns:domain=\"urn:vfi-sapphire:np.domain.2001-07-01\" xmlns:vs=\"urn:vfi-sapphire:vs.2001-10-01\">    <SUCCESS>  <serialNum>DEMO123</serialNum>        <vendor>VERIFONE</vendor>        <model>COMMANDER_APT</model>        <hardwareVersion>HW 5.01.02</hardwareVersion>    </SUCCESS></domain:registerDeviceResponse>";

                    try {
                        cSocket = (SSLSocket) serverSocket.accept();
                        bis = new BufferedInputStream(cSocket.getInputStream());


                        byte[] token = new byte[1024];
                        bis.read(token);
                        xmldata = new String(token);

                        SAXParserFactory sax = SAXParserFactory.newInstance();
                        SAXParser saxParser = sax.newSAXParser();
                        TokenParser bs = new TokenParser();
                        InputSource in = new InputSource(new StringReader(xmldata.trim()));
                        saxParser.parse(in, bs);
                        Log.v("Registration :", "token received");
                        BufferedOutputStream bos = new BufferedOutputStream(cSocket.getOutputStream());
                        editor.remove("token");
                        editor.putString("token", TokenParser.getToken());
                        editor.putString("appType", TokenParser.getappType());
                        TerminalDataSet.setAppType(TokenParser.getappType());
                        editor.commit();
                        System.out.println("This is the message from the client : " + xmldata);  bos.flush();

                        bos.write(res.getBytes()); bos.close(); }
                    finally {
                        cSocket.close();
                        serverSocket.close();
                    }
                    pd.cancel();
                    dialog.dismiss();


                    /*
                     * if(backlog > 0){ serverSocket = factory.createServerSocket(port, backlog); }
                     * else { serverSocket = factory.createServerSocket(port); serverSocket. }
                     */

                }catch(Exception e){
//
//                log.info("Exception occured while creating SSL server socket: "	+ e.getMessage());
                    Log.println(Log.DEBUG,"","");
                }

            }} );thread.setName("secure TLS");
        thread.start();

    }


    }

