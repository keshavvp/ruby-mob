package com.example.rubymobile.com.example.adatper;

import android.content.Context;
import android.widget.Toast;

import com.example.rubymobile.CartFragment;
import com.example.rubymobile.HomeFragment;
import com.example.rubymobile.data.CommandType;
import com.example.rubymobile.data.TransactionViewModelFactory;
import com.example.rubymobile.ent.ItemLine;
import com.example.rubymobile.ent.TerminalDataSet;
import com.example.rubymobile.network.HttpConnectionUtil;
import com.example.rubymobile.write.Xmlwriter;
import com.google.android.material.snackbar.Snackbar;

public class FuelSaleController {
    static CustomAddedItemDataAdapter updaterAdapter;
    Context contxt;

    public static void setUpdaterAdapter(CustomAddedItemDataAdapter ad){
        updaterAdapter = ad;
    }
    public  FuelSaleController(Context contxt){
        this.contxt = contxt;
    }

    public void addFuelItemLine(ItemLine fuelItem){
//        Toast.makeText(contxt,fuelItem.getDisplayName()+" added to cart",Toast.LENGTH_SHORT).show();
        TransactionViewModelFactory.getTransaction().getItems().add(fuelItem);
        if(updaterAdapter != null){
            updaterAdapter.notifyDataSetChanged();
            CartFragment.getInstance().showSnakBar("Sale is being evaluated");

        }

        HttpConnectionUtil util = new HttpConnectionUtil();
        Runnable run = () ->{  try{
            util.sendExtposReq(CommandType.POST_SALE, TerminalDataSet.getCookie(), Xmlwriter.getPostSaleReqPayload());
        }catch (Exception e) {
            HomeFragment.getInstance().showSnakBar(e.getMessage());
        }};
        new Thread(run).start();
    }
}
