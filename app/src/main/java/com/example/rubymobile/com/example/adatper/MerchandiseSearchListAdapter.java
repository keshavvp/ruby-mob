package com.example.rubymobile.com.example.adatper;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rubymobile.CartFragment;
import com.example.rubymobile.R;
import com.example.rubymobile.data.CommandType;
import com.example.rubymobile.data.ConfigDataViewModelFactory;
import com.example.rubymobile.data.HELPER.FileHelper;
import com.example.rubymobile.data.TransactionViewModelFactory;
import com.example.rubymobile.ent.Item;
import com.example.rubymobile.ent.ItemLine;
import com.example.rubymobile.ent.ItemSet;
import com.example.rubymobile.ent.ItemSubSetItem;
import com.example.rubymobile.ent.ItemSubset;
import com.example.rubymobile.ent.PrepayItemLine;
import com.example.rubymobile.ent.TerminalDataSet;
import com.example.rubymobile.network.HttpConnectionUtil;
import com.example.rubymobile.parsers.RefreshConfigurationResponse;
import com.example.rubymobile.write.Xmlwriter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class MerchandiseSearchListAdapter extends BaseAdapter implements Filterable{
    static CustomAddedItemDataAdapter updaterAdapter;

    ValueFilter valueFilter;
    RefreshConfigurationResponse configurationData;
    ArrayList<Item> availablePlu;
    Context contxt;
    ArrayList<ItemSet> itemSet;
    ArrayList<ItemSubSetItem> itemSubSetItem;
    List<ItemSubSetItem> searchedItems;
    List<ItemSubSetItem> allItems;
    ListView list;
    public MerchandiseSearchListAdapter(Context contxt,ListView list){
        this.list = list;
        this.contxt = contxt;
        itemSet = new ArrayList<ItemSet>();
        itemSubSetItem = new ArrayList<>();
        this.availablePlu = ConfigDataViewModelFactory.getConfiguration().getConfiguration().getPluList();
          searchedItems =getItemSubSetITems();
        allItems = itemSubSetItem;
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ItemSubSetItem itm =(ItemSubSetItem)parent.getItemAtPosition(position);
                notifyCartAdapter(itm);
            }
        });
    }

    private void notifyCartAdapter(ItemSubSetItem itm) {
        Toast.makeText(contxt,itm.getDisplayName()+" added to cart",Toast.LENGTH_SHORT).show();
/*        String  amt ="$10";
        ItemLine fuelItm = new PrepayItemLine();
        fuelItm.setDisplayName("Prepay, pos:"+1);
        String sym =  String.valueOf(amt.charAt(0));
        fuelItm.setSym(sym);
        fuelItm.setAmount(amt.substring(1));
        TransactionViewModelFactory.getTransaction().getItems().add(fuelItm);*/
        itm.setQty(1);
        TransactionViewModelFactory.getTransaction().getItems().add(itm);
        if(updaterAdapter != null){
            updaterAdapter.notifyDataSetChanged();
            CartFragment.getInstance().showSnakBar("Sale is being evaluated");

        }
        HttpConnectionUtil util = new HttpConnectionUtil();
        Runnable run = () ->{  try{
            util.sendExtposReq(CommandType.POST_SALE, TerminalDataSet.getCookie(), Xmlwriter.getPostSaleReqPayload());
        }catch (Exception e) {}};
        new Thread(run).start();
    }

    public static void setUpdaterAdapter(CustomAddedItemDataAdapter ad){
        updaterAdapter = ad;
    }
    public void notifyBArcodeReceived(String pluName){

            Optional<ItemSubSetItem>  item = allItems.stream().filter(itm -> itm.getDisplayName().toLowerCase().equals(pluName.toLowerCase())).findAny();

        if(item.isPresent()){
           notifyCartAdapter(item.get());
       }
    }
    @Override
    public int getCount() {
        return searchedItems.size();

    }


    private ArrayList<ItemSubSetItem> getItemSubSetITems(){
        ArrayList<ItemSet> set =ConfigDataViewModelFactory.getConfiguration().getConfiguration().getSetList();

        int len =set.size();
        for(int k = 0;k<len;k++){
            List<ItemSubset> itm = set.get(k).getItemSubsets();
            for(int j = 0;j< itm.size();j++){
                itemSubSetItem.addAll(itm.get(j).getsubSetItems());

            }
        }
        return itemSubSetItem;

    }

    @Override
    public Object getItem(int position) {
        return searchedItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflator =(LayoutInflater)  contxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listView = inflator.inflate(R.layout.cartlist,null);
        ImageView image = listView.findViewById(R.id.pluImage);
        TextView pluName = listView.findViewById(R.id.pluName);
//            TextView desc = listView.findViewById(R.id.pluDesc);
        TextView price = listView.findViewById(R.id.price);
        ItemSubSetItem item =  searchedItems.get(position);
        File file = new File(contxt.getExternalFilesDir("RubyMobile"),item.getIconName());

//        File f =new File( FileHelper.path+"/images/"+item.getIconName());
        image.setImageURI(Uri.fromFile(file));//ImageResource(R.drawable.burger1);
        pluName.setLeft(1);
//            pluName.setText("Item F                   $9.99");
        pluName.setText(item.getDisplayName());
//            desc.setText(item.getUPC());
        LinearLayout lay = listView.findViewById(R.id.qtyViw);
        lay.setVisibility(View.GONE);


        String amount="";
        String sym="";

           for(Item itm :availablePlu){
                if(itm.getUPC().equals(item.getUPC()) && itm.getModifier().equals(item.getModifier())){
                    amount = itm.getAmount().toString();
                    sym = itm.getSymbol();
                    break;
                }
            }
//            total += Double.valueOf(amount);
        price.setText(sym + amount);
 //            bottomSheetTotal(sym);
//            TransactionViewModelFactory.getTransaction().setItems(addedItems);


//            price.setText(addedItems.get(position).get());
        return listView;
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                List<ItemSubSetItem> filterList = new ArrayList<>();
                for (int i = 0; i < allItems.size(); i++) {
                    if ((allItems.get(i).getDisplayName().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(allItems.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = allItems.size();
                results.values = allItems;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            searchedItems = (List<ItemSubSetItem>) results.values;
            if(searchedItems.size()==0){
                Toast.makeText(contxt,constraint + " Not Found",Toast.LENGTH_SHORT).show();
            }
            notifyDataSetChanged();
        }

    }
}
