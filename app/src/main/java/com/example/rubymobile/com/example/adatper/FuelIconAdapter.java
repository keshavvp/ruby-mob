package com.example.rubymobile.com.example.adatper;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.example.rubymobile.R;
import com.example.rubymobile.ent.DeviceClass;
import com.example.rubymobile.ent.DeviceClassData;
import com.example.rubymobile.ent.FPStatusEvent;
import com.example.rubymobile.ent.IFuelEvent;
import com.example.rubymobile.handler.FuelSSEEventHandler;
import com.example.rubymobile.parsers.VistConstants;

import java.util.ArrayList;

public class FuelIconAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<View> pumpsView = new ArrayList<>();
    private static final String icon = "IMAGE_ICON";
    private static final String stop = "STOP";
    private static final String txt = "STATUS";
    private static final String PUMP_NUMBER = "pump_number";
    private static final String due_1 = "due_amount1";
    private static final String due_2 = "due_amount2";
    private GridView gridView;
    FragmentActivity ctx;

    // Constructor
    public FuelIconAdapter(Context c, GridView gridView) {
        mContext = c;
        this.gridView = gridView;
        ctx = (FragmentActivity) c;
        FuelSSEEventHandler.setObserver(this);
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return gridView.getChildAt(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.fuel_icon, null);
        view.setBackgroundColor(Color.TRANSPARENT);
        TextView im = view.findViewById(R.id.fpPosNum);
        im.setText(String.valueOf(position + 1));

        TextView fpStatus = view.findViewById(R.id.fpStatus);
        fpStatus.setText("idle");
        fpStatus.setTag(txt);

        TextView pumpNumber = view.findViewWithTag(PUMP_NUMBER);
        TextView text = view.findViewWithTag(txt);

        ImageView iv = view.findViewById(R.id.icon);
        iv.setTag(icon);

        ImageView stop_im = view.findViewById(R.id.stop);
        stop_im.setTag(stop);

        iv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_fuel_pump_icon));
        iv.setColorFilter(Color.parseColor("#388E3C"));//white
        pumpNumber.setTextColor(Color.parseColor("#388E3C"));
        text.setTextColor(Color.parseColor("#388E3C"));
        if (pumpsView.size() == 16) {
            pumpsView.clear();
        }
        pumpsView.add(view);


        return view;
    }

    // Keep all Images in array
    public Integer[] mThumbIds = {
            R.drawable.ic_fuel_pump_icon, R.drawable.ic_fuel_pump_icon,
            R.drawable.ic_fuel_pump_icon, R.drawable.ic_fuel_pump_icon,
            R.drawable.ic_fuel_pump_icon, R.drawable.ic_fuel_pump_icon,
            R.drawable.ic_fuel_pump_icon, R.drawable.ic_fuel_pump_icon,
            R.drawable.ic_fuel_pump_icon, R.drawable.ic_fuel_pump_icon,
            R.drawable.ic_fuel_pump_icon,
            R.drawable.ic_fuel_pump_icon,
            R.drawable.ic_fuel_pump_icon,
            R.drawable.ic_fuel_pump_icon,
            R.drawable.ic_fuel_pump_icon,
            R.drawable.ic_fuel_pump_icon,
    };

    public void updateColors(int i) {
        View v = pumpsView.get(i);
        ImageView image = v.findViewWithTag(icon);

        image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_fuel_pump_icon));
        image.setColorFilter(Color.parseColor("#00E676"));
        this.notifyDataSetChanged();
    }

    public void notifySSEEvent(IFuelEvent event) {
        if (event instanceof FPStatusEvent) {
            updateColors(event);
        }
    }

    public void updateColors(View view) {
        ImageView image = view.findViewWithTag(icon);

        image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_fuel_pump_icon));
        image.setColorFilter(Color.parseColor("#2F9A2B"));

        TextView text = view.findViewWithTag(txt);
        text.setText("Reserving");
        text.setBackgroundColor(Color.WHITE);

    }

    public void updateColors(View view, int position) {
        ImageView image = view.findViewWithTag(icon);
        TextView pumpNumber = view.findViewWithTag(PUMP_NUMBER);
        image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_fuel_pump_icon));
        TextView text = view.findViewWithTag(txt);

 /*       if(position == 2){
            image.setColorFilter(Color.parseColor("#E6811A")); //red
            text.setText("Reserved");
            text.setTextColor(Color.parseColor("#E6811A"));//red


        }*/
        if (position == 3) {
            image.setColorFilter(Color.parseColor(mContext.getString(R.string.colorPumpReserved))); //red
            text.setText("Reserved");
            ((RelativeLayout) ((RelativeLayout) ((RelativeLayout) view).getChildAt(0)).
                    getChildAt(0)).setBackgroundColor(Color.parseColor(mContext.getString(R.string.colorPumpReserved)));
            image.setColorFilter(Color.parseColor(mContext.getString(R.string.colorWhite)));//white
            text.setTextColor(Color.parseColor(mContext.getString(R.string.colorWhite)));//white
            text.setText("Reserved");
//            line.setBackgroundColor(Color.parseColor(mContext.getString(R.string.colorWhite)));
            pumpNumber.setTextColor(Color.parseColor(mContext.getString(R.string.colorWhite)));//white
        }
      /*  else{
            ((RelativeLayout)((RelativeLayout)((RelativeLayout)view).getChildAt(0)).
                    getChildAt(0)).setBackgroundColor(Color.parseColor("#388E3C"));//green
            image.setColorFilter(Color.parseColor("#FFFFFF"));//white
            text.setTextColor(Color.parseColor("#FFFFFF"));//white
            text.setText("Dispencing");
            line.setBackgroundColor(Color.parseColor("#FFFFFF"));
            pumpNumber.setTextColor(Color.parseColor("#FFFFFF"));//white
        }*/
//        text.setTextColor(Color.WHITE);


    }

    public void updateColors(IFuelEvent event) {
        Runnable uiThread = () -> ctx.runOnUiThread(new Runnable() {


            @Override
            public void run() {

                // Stuff that updates the UI

                if (event instanceof FPStatusEvent) {
                    FPStatusEvent ev = (FPStatusEvent) event;
                    for (DeviceClass dev : ev.getFpStatus()) {
                        if (dev != null && Integer.parseInt(dev.getDeviceID()) > 16) {
                            continue;
                        }

                        RelativeLayout fp = (RelativeLayout) getItem(Integer.parseInt(dev.getDeviceID()) - 1);

                        ImageView image = fp.findViewWithTag(icon);
                        TextView pumpNumber = fp.findViewWithTag(PUMP_NUMBER);
                        image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_fuel_pump_icon));
                        TextView text = fp.findViewWithTag(txt);
                        ImageView stopImage = fp.findViewWithTag(stop);
                         TextView due_1_txt = fp.findViewWithTag(due_1);
                        TextView due_2_txt = fp.findViewWithTag(due_2);

                        if (dev.getDeviceState().equals(VistConstants.FDC_RESERVED) ||
                                dev.getDeviceState().equals(VistConstants.FDC_CALLING)

                        ) {
                            image.setColorFilter(Color.parseColor("#E6811A")); //red
                            text.setText("Reserved");
                            ((RelativeLayout) ((RelativeLayout) ((RelativeLayout) fp).getChildAt(0)).
                                    getChildAt(0)).setBackgroundColor(Color.parseColor("#E6811A"));
                            image.setColorFilter(Color.WHITE);//white
                            text.setTextColor(Color.WHITE);//white
                            text.setText("Reserved");
                            stopImage.setVisibility(View.GONE);
                             pumpNumber.setTextColor(Color.WHITE);//white
                        } else if (dev.getDeviceState().equals(VistConstants.FDC_DISABLED)
                        ) {
                            image.setColorFilter(Color.BLACK); //red
                            text.setText("Disabled");
                            ((RelativeLayout) ((RelativeLayout) ((RelativeLayout) fp).getChildAt(0)).
                                    getChildAt(0)).setBackgroundColor(Color.WHITE);
                            image.setColorFilter(Color.GRAY);//white
                            stopImage.setVisibility(View.VISIBLE);
                            text.setTextColor(Color.RED);//white
                            text.setText("Stop");
                            due_1_txt.setVisibility(View.GONE);
                            due_2_txt.setVisibility(View.GONE);
                             pumpNumber.setTextColor(Color.BLACK);//white
                        } else if (dev.getDeviceState().equals(VistConstants.FDC_FUELLING)) {
                            ((RelativeLayout) ((RelativeLayout) ((RelativeLayout) fp).getChildAt(0)).
                                    getChildAt(0)).setBackgroundColor(Color.parseColor("#388E3C"));//green
                            image.setColorFilter(Color.parseColor("#FFFFFF"));//white
                            text.setTextColor(Color.parseColor("#FFFFFF"));//white
                            text.setText("Dispensing");
                            stopImage.setVisibility(View.GONE);
                             pumpNumber.setTextColor(Color.parseColor("#FFFFFF"));//white
                        } else if (dev.getDeviceState().equals(VistConstants.FDC_READY)) {
                            ((RelativeLayout) ((RelativeLayout) ((RelativeLayout) fp).getChildAt(0)).
                                    getChildAt(0)).setBackgroundColor(Color.parseColor("#FFFFFF"));//green "#388E3C
                            image.setColorFilter(Color.parseColor("#388E3C"));//white
                            text.setTextColor(Color.parseColor("#388E3C"));//white
                            text.setText("Idle");
                            stopImage.setVisibility(View.GONE);
                             pumpNumber.setTextColor(Color.parseColor("#388E3C"));//white
                        } else if (dev.getDeviceState().equals(VistConstants.FDC_AUTHORIZED)) {
                            /*((RelativeLayout) ((RelativeLayout) ((RelativeLayout) fp).getChildAt(0)).
                                    getChildAt(0)).setBackgroundColor(Color.parseColor("#FFFFFF"));//green "#388E3C
                            image.setColorFilter(Color.parseColor("#388E3C"));//white
                            text.setTextColor(Color.parseColor("#388E3C"));//white
*/
                            text.setText("Authorized");
                            stopImage.setVisibility(View.GONE);
                             pumpNumber.setTextColor(Color.parseColor("#388E3C"));//white
                        }
                    }
                }
            }
        });
        new Thread(uiThread).start();
    }

    public void notifyFuelSaleTrx(ArrayList<DeviceClassData> deviceClasses) {
        Runnable uiThread = () -> ctx.runOnUiThread(new Runnable() {


            @Override
            public void run() {

                // Stuff that updates the UI
                for (DeviceClassData dev : deviceClasses) {
                    if (dev != null && Integer.parseInt(dev.getDeviceID()) > 16) {
                        continue;
                    }


                    RelativeLayout fp = (RelativeLayout) getItem(Integer.parseInt(dev.getDeviceID()) - 1);
                    TextView due_1_txt = fp.findViewWithTag(due_1);
                    TextView due_2_txt = fp.findViewWithTag(due_2);
                    if (dev.getState1().equalsIgnoreCase(VistConstants.locked) ||
                            dev.getState1().equalsIgnoreCase(VistConstants.payable)
                    ) {
                        due_1_txt.setVisibility(View.VISIBLE);

                        due_1_txt.setText(getformatAmount(dev.getAmount1()));

                    } else {
                        due_1_txt.setVisibility(View.GONE);
                    }
                    if (dev.getState2().equalsIgnoreCase(VistConstants.locked) ||
                            dev.getState2().equalsIgnoreCase(VistConstants.payable)
                    ) {
                        due_2_txt.setVisibility(View.VISIBLE);
                        due_2_txt.setText(getformatAmount(dev.getAmount2()));
                    } else {
                        due_2_txt.setVisibility(View.GONE);

                    }
                }
            }

            private String getformatAmount(String amount1) {
                if (amount1.equals("0")) {
                    return "0.0";
                } else return amount1;
            }
        });
        new Thread(uiThread).start();
    }
}
