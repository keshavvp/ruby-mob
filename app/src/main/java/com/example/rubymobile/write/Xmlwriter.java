package com.example.rubymobile.write;


import android.util.Log;

import com.example.rubymobile.data.TransactionViewModelFactory;
import com.example.rubymobile.ent.ItemLine;
import com.example.rubymobile.ent.ItemSubSetItem;
import com.example.rubymobile.ent.Payment;
import com.example.rubymobile.ent.PostPay;
import com.example.rubymobile.ent.PrepayItemLine;
import com.example.rubymobile.ent.TerminalDataSet;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class Xmlwriter {

    public static String getBeginSessionXml() {
        StringWriter stringWriter = new StringWriter();
        String xmlString = null;

        try {
            XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xMLStreamWriter =
                    xMLOutputFactory.createXMLStreamWriter(stringWriter);

            xMLStreamWriter.writeStartDocument();
            xMLStreamWriter.writeStartElement("r:salesProcessorCmd");
            xMLStreamWriter.writeAttribute("xmlns:r", "urn:vfi-extpos.2001-10-01");

            xMLStreamWriter.writeStartElement("beginSession");


            xMLStreamWriter.writeStartElement("externalUserInformation");
            xMLStreamWriter.writeAttribute("version", "1.0.0");
            xMLStreamWriter.writeStartElement("userID");

            xMLStreamWriter.writeCharacters(TerminalDataSet.getUser().getUserId());
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeStartElement("userPasswd");

            xMLStreamWriter.writeCharacters(TerminalDataSet.getUser().getPassword());
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeStartElement("terminalIdentifier");
            xMLStreamWriter.writeAttribute("application", TerminalDataSet.getAppType());
            xMLStreamWriter.writeCharacters(TerminalDataSet.getTerminalid());
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeStartElement("terminalInfo");

            xMLStreamWriter.writeStartElement("application");
            xMLStreamWriter.writeAttribute("buildDate", "2018-01-26T15:43:50+05:00");
            xMLStreamWriter.writeAttribute("name", "CarbonSelfCheckout");
            xMLStreamWriter.writeAttribute("version", "1.0");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeStartElement("os");
            xMLStreamWriter.writeAttribute("buildDate", "2018-01-26T15:43:50+05:00");
            xMLStreamWriter.writeAttribute("name", "Android");
            xMLStreamWriter.writeAttribute("version", "1.10");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();


            xMLStreamWriter.writeStartElement("communicationMode");
            xMLStreamWriter.writeCharacters("ASYNCHRONOUS");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();


            xMLStreamWriter.writeEndDocument();

            xMLStreamWriter.flush();
            xMLStreamWriter.close();

            xmlString = stringWriter.getBuffer().toString();

            stringWriter.close();

            System.out.println(xmlString);

        } catch (Exception e) {
        }
        return xmlString;
    }

    public static String getfdcLoginReq() {
        StringWriter stringWriter = new StringWriter();
        String xmlString = null;

        try {
            XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xMLStreamWriter =
                    xMLOutputFactory.createXMLStreamWriter(stringWriter);

            xMLStreamWriter.writeStartDocument();
            xMLStreamWriter.writeStartElement("LogOnRequest");
            xMLStreamWriter.writeAttribute("xmlns", "http://www.pcats.org/schema/ifsf/fdc");
            xMLStreamWriter.writeAttribute("ApplicationSender", "POS");
            xMLStreamWriter.writeAttribute("WorkstationID", TerminalDataSet.getTerminalid());
            xMLStreamWriter.writeAttribute("RequestID", "POS171999");


            xMLStreamWriter.writeStartElement("POSdata");

            xMLStreamWriter.writeStartElement("POSTimeStamp");
            xMLStreamWriter.writeCharacters(getFormattedDate(System.currentTimeMillis()));
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeStartElement("InterfaceVersion");
            xMLStreamWriter.writeCharacters("V2.2");
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeStartElement("AlertCapable");
            xMLStreamWriter.writeCharacters("false");
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeStartElement("UnsolicitedMessageCapable");
            xMLStreamWriter.writeCharacters("true");
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeEndDocument();

            xMLStreamWriter.flush();
            xMLStreamWriter.close();
            xmlString = stringWriter.getBuffer().toString();
            stringWriter.close();
            System.out.println(xmlString);
        } catch (Exception e) {
            Log.d("writing xml", "error in writing post payload" + e);
        }
        return xmlString;
    }


    public static String getOpenCashierXmlPayload() {
        StringWriter stringWriter = new StringWriter();
        String xmlString = null;

        try {
            XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xMLStreamWriter =
                    xMLOutputFactory.createXMLStreamWriter(stringWriter);

            xMLStreamWriter.writeStartDocument();
            xMLStreamWriter.writeStartElement("r:tillProcessorCmd");
            xMLStreamWriter.writeAttribute("xmlns:r", "urn:vfi-extpos.2001-10-01");

            xMLStreamWriter.writeStartElement("openCashier");

            xMLStreamWriter.writeStartElement("externalSessionIdentifier");
            xMLStreamWriter.writeCharacters(TerminalDataSet.getextSessionID());
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeStartElement("beginCashAmount");
            xMLStreamWriter.writeAttribute("currency", "USD");
            xMLStreamWriter.writeCharacters("0");
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeStartElement("drawer");
            xMLStreamWriter.writeAttribute("number", "1");
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndDocument();

            xMLStreamWriter.flush();
            xMLStreamWriter.close();
            xmlString = stringWriter.getBuffer().toString();
            stringWriter.close();
            System.out.println(xmlString);
        } catch (Exception e) {

        }
        return xmlString;
    }

    public static String getendSessionPayload() {
        StringWriter stringWriter = new StringWriter();
        String xmlString = null;

        try {
            XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xMLStreamWriter =
                    xMLOutputFactory.createXMLStreamWriter(stringWriter);

            xMLStreamWriter.writeStartDocument();
            xMLStreamWriter.writeStartElement("r:salesProcessorCmd");
            xMLStreamWriter.writeAttribute("xmlns:r", "urn:vfi-extpos.2001-10-01");

            xMLStreamWriter.writeStartElement("endSession");

            xMLStreamWriter.writeStartElement("externalSessionIdentifier");
            xMLStreamWriter.writeCharacters(TerminalDataSet.getextSessionID());
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.flush();
            xMLStreamWriter.close();
            xmlString = stringWriter.getBuffer().toString();
            stringWriter.close();
            System.out.println(xmlString);
        } catch (Exception e) {

        }
        return xmlString;
    }

    public static String getVoidPayload() {
        StringWriter stringWriter = new StringWriter();
        String xmlString = null;

        try {
            XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xMLStreamWriter =
                    xMLOutputFactory.createXMLStreamWriter(stringWriter);

            xMLStreamWriter.writeStartDocument();
            xMLStreamWriter.writeStartElement("r:salesProcessorCmd");
            xMLStreamWriter.writeAttribute("xmlns:r", "urn:vfi-extpos.2001-10-01");

            xMLStreamWriter.writeStartElement("unpostFinancialTxn");

            xMLStreamWriter.writeStartElement("externalSessionIdentifier");
            xMLStreamWriter.writeCharacters(TerminalDataSet.getextSessionID());
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.flush();
            xMLStreamWriter.close();
            xmlString = stringWriter.getBuffer().toString();
            stringWriter.close();
            System.out.println(xmlString);
        } catch (Exception e) {

        }
        return xmlString;
    }

    public static String getSuspendCashierXmlPayload() {
        StringWriter stringWriter = new StringWriter();
        String xmlString = null;

        try {
            XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xMLStreamWriter =
                    xMLOutputFactory.createXMLStreamWriter(stringWriter);

            xMLStreamWriter.writeStartDocument();
            xMLStreamWriter.writeStartElement("r:salesProcessorCmd");
            xMLStreamWriter.writeAttribute("xmlns:r", "urn:vfi-extpos.2001-10-01");

            xMLStreamWriter.writeStartElement("suspendFinancialTxn");

            xMLStreamWriter.writeStartElement("externalSessionIdentifier");
            xMLStreamWriter.writeCharacters(TerminalDataSet.getextSessionID());
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.flush();
            xMLStreamWriter.close();
            xmlString = stringWriter.getBuffer().toString();
            stringWriter.close();
            System.out.println(xmlString);
        } catch (Exception e) {

        }
        return xmlString;
    }

    public static String getRefreshConfigXmlPayload() {
        StringWriter stringWriter = new StringWriter();
        String xmlString = null;

        try {
            XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xMLStreamWriter =
                    xMLOutputFactory.createXMLStreamWriter(stringWriter);

            xMLStreamWriter.writeStartDocument();
            xMLStreamWriter.writeStartElement("r:salesProcessorCmd");
            xMLStreamWriter.writeAttribute("xmlns:r", "urn:vfi-extpos.2001-10-01");

            xMLStreamWriter.writeStartElement("refreshConfiguration");

            xMLStreamWriter.writeStartElement("externalSessionIdentifier");
            xMLStreamWriter.writeCharacters(TerminalDataSet.getextSessionID());
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.flush();
            xMLStreamWriter.close();
            xmlString = stringWriter.getBuffer().toString();
            stringWriter.close();
            System.out.println(xmlString);
        } catch (Exception e) {

        }
        return xmlString;
    }

    public static String getPostSaleReqPayload() {
        StringWriter stringWriter = new StringWriter();
        String xmlString = null;

        try {
            XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xMLStreamWriter =
                    xMLOutputFactory.createXMLStreamWriter(stringWriter);

            xMLStreamWriter.writeStartDocument();
            xMLStreamWriter.writeStartElement("r:salesProcessorCmd");
            xMLStreamWriter.writeAttribute("xmlns:r", "urn:vfi-extpos.2001-10-01");

            xMLStreamWriter.writeStartElement("postFinancialTxn");

            xMLStreamWriter.writeStartElement("externalSessionIdentifier");
            xMLStreamWriter.writeCharacters(TerminalDataSet.getextSessionID());
            xMLStreamWriter.writeEndElement();

            xMLStreamWriter.writeStartElement("externalSale");
            xMLStreamWriter.writeAttribute("type", "SALE");

            ArrayList<ItemLine> items = TransactionViewModelFactory.getTransaction().getItems();
            for (ItemLine itm : items) {
                xMLStreamWriter.writeStartElement("item");
                if (itm instanceof ItemSubSetItem) {
                    xMLStreamWriter.writeStartElement("plu");
                    xMLStreamWriter.writeAttribute("modifier", itm.getModifier());
                    xMLStreamWriter.writeAttribute("upc", itm.getUPC());
                    xMLStreamWriter.writeStartElement("quantity");
                    xMLStreamWriter.writeCharacters(String.valueOf(itm.getQty()));
                    xMLStreamWriter.writeEndElement();
                    xMLStreamWriter.writeEndElement();
                } else {
                    if (itm instanceof PrepayItemLine) {
                        PrepayItemLine il = (PrepayItemLine) itm;
                        xMLStreamWriter.writeStartElement("prepayFuel");

                        xMLStreamWriter.writeStartElement("fuelingPosition");
                        xMLStreamWriter.writeCharacters(String.valueOf((il.getFuelingPosition())));
                        xMLStreamWriter.writeEndElement();

                        xMLStreamWriter.writeStartElement("amountLimit");
                        xMLStreamWriter.writeCharacters(String.valueOf((il.getAmount())));
                        xMLStreamWriter.writeEndElement();

                        xMLStreamWriter.writeStartElement("grade");
                        xMLStreamWriter.writeCharacters("1");
                        xMLStreamWriter.writeEndElement();


                        xMLStreamWriter.writeEndElement();
                    }
                }
                if(itm instanceof PostPay) {
                    xMLStreamWriter.writeStartElement("postpayFuel");

                    xMLStreamWriter.writeStartElement("fuelingPosition");
                    xMLStreamWriter.writeCharacters(((PostPay) itm).getPositionNum());
                    xMLStreamWriter.writeEndElement();

                    xMLStreamWriter.writeEndElement();

                }
                xMLStreamWriter.writeEndElement();
            }
            ArrayList<Payment> payment = TransactionViewModelFactory.getTransaction().getPayment();
            for (Payment pay : payment) {
                xMLStreamWriter.writeStartElement("payment");
                xMLStreamWriter.writeAttribute("sysid", pay.getSysid());
                xMLStreamWriter.writeStartElement("amount");
                xMLStreamWriter.writeCharacters("0");

                xMLStreamWriter.writeEndElement();
                xMLStreamWriter.writeEndElement();

            }
            xMLStreamWriter.writeStartElement("receiptTextReqd");
            xMLStreamWriter.writeCharacters("true");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndDocument();

            xMLStreamWriter.flush();
            xMLStreamWriter.close();
            xmlString = stringWriter.getBuffer().toString();
            stringWriter.close();
            System.out.println(xmlString);
        } catch (Exception e) {
            Log.d("writing xml", "error in writing post payload" + e);
        }
        return xmlString;
    }
    private static String getFormattedDate(Long timeStamp)   {
        String  datePattern = "yyyy-MM-dd'T'HH:mm:ss";
        Date date =new Date(timeStamp);
        SimpleDateFormat dateFormatter =new  SimpleDateFormat(datePattern);
        return   dateFormatter.format(date);
    }
    public static void main(String[] a) {
        getPostSaleReqPayload();
    }
}
