package com.example.rubymobile.eventHandler;

import android.app.AlertDialog;
import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.example.rubymobile.R;
import com.example.rubymobile.com.example.adatper.FuelSaleController;
import com.example.rubymobile.ent.ItemLine;
import com.example.rubymobile.ent.PostPay;
import com.example.rubymobile.ent.PrepayItemLine;
import com.example.rubymobile.network.HttpConnectionUtil;

import org.jetbrains.annotations.NotNull;

public class FuelSaleClickHandler implements AdapterView.OnItemClickListener , View.OnClickListener {
    Context context;
    AlertDialog menuDialog;
    AlertDialog inputMenuDialg;
    FuelSaleController ctrl;
    public FuelSaleClickHandler(Context context,FuelSaleController ctrl , AlertDialog menuDialog,AlertDialog inputMenuDialg){
        this.context = context;
        this.menuDialog =menuDialog;
        this.ctrl = ctrl;
     }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        AlertDialog.Builder menuItem = new AlertDialog.Builder(context);
        AlertDialog.Builder inputMenItem = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View fuelMenu = inflater.inflate(R.layout.custom_menu_items_view, null);
        TextView header = fuelMenu.findViewById(R.id.header);
        header.setText("Fueling Position "+ (position + 1));
        Button btn1 =  fuelMenu.findViewById(R.id.option1);
        btn1.setVisibility(View.VISIBLE);
        btn1.setText("Prepay");
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuDialog.dismiss();
                LayoutInflater inflater = LayoutInflater.from(context);
                View inputMenu = inflater.inflate(R.layout.custom_input_menu_items, null);
                TextView header = inputMenu.findViewById(R.id.header);
                header.setText("Fuel Parameters");
                TextView lfpNum =  inputMenu.findViewById(R.id.label1);
                lfpNum.setText("Enter Position Number");
                TextView fpNum =  inputMenu.findViewById(R.id.inputText1);
                fpNum.setText(String.valueOf(position + 1));
                fpNum.setClickable(false);

                fpNum.setInputType(InputType.TYPE_NULL);

                TextView lAmt =  inputMenu.findViewById(R.id.label2);
                lAmt.setText("Enter Amount");
                TextView amt =  inputMenu.findViewById(R.id.inputText2);
                amt.setInputType((InputType.TYPE_CLASS_NUMBER + InputType.TYPE_NUMBER_FLAG_DECIMAL));
                lAmt.setVisibility(View.VISIBLE);
                TextView amT =  inputMenu.findViewById(R.id.inputText2);
                amT.setVisibility(View.VISIBLE);
                Button ok =  inputMenu.findViewById(R.id.ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        inputMenuDialg.dismiss();
                        addPrepayITem(amT.getText().toString(), String.valueOf(position+1));
                    }
                });

                Button cancel =  inputMenu.findViewById(R.id.cancel);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        inputMenuDialg.dismiss();
                    }
                });
                inputMenItem.setView(inputMenu);
                inputMenuDialg = inputMenItem.create();inputMenuDialg.show();
            }
        });

        Button btn2 =  fuelMenu.findViewById(R.id.option2);
        btn2.setVisibility(View.VISIBLE);
        btn2.setText("Claim Due");
        btn2.setOnClickListener(claimFuel(position ));
        Button btn3 =  fuelMenu.findViewById(R.id.option3);
        btn3.setVisibility(View.VISIBLE);
        btn3.setText("Preset");

        menuItem.setView(fuelMenu);
        menuDialog = menuItem.create();
        menuDialog.show();

    }

    private void addPrepayITem(String amount, String fPos) {
            PrepayItemLine fuelItm = new PrepayItemLine();
            fuelItm.setDisplayName("Prepay    #"+fPos);
            String sym =  String.valueOf('$');
            fuelItm.setSym(sym);
            fuelItm.setFuelingPosition(fPos);
            fuelItm.setAmount(amount);
            ctrl.addFuelItemLine(fuelItm);

    }
    @NotNull
    private View.OnClickListener claimFuel(int position) {
        return v -> {

            menuDialog.dismiss();
            PostPay pay = new PostPay();
            int pos = position + 1;
            pay.setPositionNum(String.valueOf(pos));
            pay.setDisplayName("Claiming    #" + pos);
            ctrl.addFuelItemLine(pay);

        };
    }
    @Override
    public void onClick(View v) {
        AlertDialog.Builder menuItem = new AlertDialog.Builder(context);
        AlertDialog.Builder inputMenItem = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View fuelMenu = inflater.inflate(R.layout.custom_menu_items_view, null);
        Button btn1 =  fuelMenu.findViewById(R.id.option1);
        TextView header = fuelMenu.findViewById(R.id.header);
        header.setText("Fuel Sale Type");
        btn1.setVisibility(View.VISIBLE);
        btn1.setText("Prepay");
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuDialog.dismiss();
                LayoutInflater inflater = LayoutInflater.from(context);
                View inputMenu = inflater.inflate(R.layout.custom_input_menu_items, null);
                TextView header = inputMenu.findViewById(R.id.header);
                header.setText("Fuel Parameters");
                TextView lfpNum =  inputMenu.findViewById(R.id.label1);
                lfpNum.setText("Enter Position Number");
                TextView position =  inputMenu.findViewById(R.id.inputText1);

                TextView lAmt =  inputMenu.findViewById(R.id.label2);
                lAmt.setText("Enter Amount");
                TextView amt =  inputMenu.findViewById(R.id.inputText2);
                amt.setInputType((InputType.TYPE_CLASS_NUMBER + InputType.TYPE_NUMBER_FLAG_DECIMAL));

                lAmt.setVisibility(View.VISIBLE);
                TextView amT =  inputMenu.findViewById(R.id.inputText2);
                amT.setVisibility(View.VISIBLE);
                Button ok =  inputMenu.findViewById(R.id.ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        inputMenuDialg.dismiss();
                        addPrepayITem(amT.getText().toString(),position.getText().toString());
                    }
                });
                Button cancel =  inputMenu.findViewById(R.id.cancel);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        inputMenuDialg.dismiss();
                    }
                });
                inputMenItem.setView(inputMenu);
                inputMenuDialg = inputMenItem.create();
                inputMenuDialg.show();
            }
        });
        Button btn2 =  fuelMenu.findViewById(R.id.option2);
        btn2.setVisibility(View.VISIBLE);
        btn2.setText("Postpay");

        Button btn3 =  fuelMenu.findViewById(R.id.option3);
        btn3.setVisibility(View.VISIBLE);
        btn3.setText("Preset");

               /* final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setView(fuelMenu);
                AlertDialog dialog = alert.create();alert.show();*/

        menuItem.setView(fuelMenu);
        menuDialog = menuItem.create();
        menuDialog.show();

    }
}
