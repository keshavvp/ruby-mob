package com.example.rubymobile.controller;

import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


import com.example.rubymobile.CartFragment;
import com.example.rubymobile.R;
import com.example.rubymobile.ent.DisplayMessageType;
import com.example.rubymobile.ent.IPromptReq;
import com.google.android.material.snackbar.Snackbar;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class PromptController   {
    static PromptController myInstance;

    static Runnable servePrompt;
    static AlertDialog.Builder alert;
    static AlertDialog dialog;
    static final ExecutorService executor = Executors.newFixedThreadPool(10);
    static Handler handler =  new Handler();
    static View promptsView;
    PromptController(){


    }
   static AppCompatActivity contxt;
   public static PromptController getMyInstance(AppCompatActivity con){
        if(myInstance == null){
            myInstance = new PromptController();
        }
        contxt = con;
       alert = new AlertDialog.Builder(contxt);
       return myInstance;

    }

    public static void clear(){

             if(dialog!=null){
                 CartFragment.getInstance().enablePAyGird(true);
                 dialog.dismiss();
             }
     }
    public static void handlePorompt(IPromptReq req){
         servePrompt =() ->{
            contxt.runOnUiThread(new Runnable() {
                public void run() {
                    /*Intent i = new Intent(actInstance, PromptActivity.class);
                    startActivity(i);*/
                    if(dialog!=null){
                         dialog.dismiss();
                    }
                    handler.removeCallbacks(servePrompt);
                    handler.removeCallbacksAndMessages(servePrompt);
                    handler.removeCallbacksAndMessages(null);

                    LayoutInflater li = LayoutInflater.from(contxt);
                    promptsView = li.inflate(R.layout.prompt_view, null);

                    TextView header = promptsView.findViewById(R.id.header);
                    TextView msg1 = promptsView.findViewById(R.id.message1);
                    TextView msg2 = promptsView.findViewById(R.id.message2);

                    header.setText(req.getHeader());


                    msg1.setText(req.getMsg().get(0));
                    try{ if(req.getMsg().get(1) != null){
                        msg2.setText(req.getMsg().get(1));
                    }}catch (Exception e){}
                    Button btn = promptsView.findViewById(R.id.okRcancel);


                    if(req.isCancellable()){
                        btn.setText("CANCEL");
                        btn.setBackgroundColor(Color.parseColor("F44336"));
                    }else if(! (req instanceof DisplayMessageType)){
                        btn.setText("OK");
                        btn.setBackgroundColor(Color.parseColor("#00CCFF"));
                    }else{
                        btn.setVisibility(View.INVISIBLE);
                    }
                    if(dialog != null){
                        dialog.dismiss();
                    }
                    long timeinMs;
                 /*   alert.setCancelable(false);

                    contxt.setFinishOnTouchOutside(false);*/

                    alert.setView(promptsView);
                    dialog =null;

                   dialog  = alert.create();


                    if(req.getTimeOut() >0){
                      /*  timeinMs =  TimeUnit.SECONDS.toMillis(req.getTimeOut());

                        handler.postDelayed(new Runnable() {

                            public void run() {

                                dialog.dismiss();
                            }
                        }, timeinMs);*/
                    }

                    dialog.show();

//                    Toast.makeText(actInstance.getApplicationContext(),"Toast text",Toast.LENGTH_SHORT).show();
                }
            });

        };
         executor.submit(servePrompt);
     }

    public void   showSnakBar(String messages){

//        Snackbar.make(contxt.getV(), messages, Snackbar.LENGTH_LONG).show();
    }
    public static void ShowPrompt(IPromptReq req){
        servePrompt =() ->{
            contxt.runOnUiThread(new Runnable() {
                public void run() {
                    handler.removeCallbacks(servePrompt);
                    handler.removeCallbacksAndMessages(servePrompt);
                    handler.removeCallbacksAndMessages(null);

                    LayoutInflater li = LayoutInflater.from(contxt);
                    promptsView = li.inflate(R.layout.prompt_view, null);

                    TextView header = promptsView.findViewById(R.id.header);
                    TextView msg1 = promptsView.findViewById(R.id.message1);
                    TextView msg2 = promptsView.findViewById(R.id.message2);

                    header.setText(req.getHeader());

                    try{
                        msg1.setText(req.getMsg().get(0));

                        if(req.getMsg().get(1) != null){
                        msg2.setText(req.getMsg().get(1));
                    }}catch (Exception e){}
                    Button btn = promptsView.findViewById(R.id.okRcancel);


                    if(req.isCancellable()){
                        btn.setText("CANCEL");
                        btn.setBackgroundColor(Color.RED);
                    }else{
                        btn.setVisibility(View.INVISIBLE);
                    }
                    if(dialog != null){
                        dialog.dismiss();
                    }
                     alert.setView(promptsView);
                    dialog =null;

                    dialog  = alert.create();
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    if(req.getTimeOut() >0){

                    }

                    dialog.show();

                 }
            });

        };
        executor.submit(servePrompt);
    }



}
