package com.example.rubymobile.data;

import androidx.lifecycle.ViewModel;


import com.example.rubymobile.ent.ItemLine;
import com.example.rubymobile.ent.ItemSubSetItem;
import com.example.rubymobile.ent.Payment;

import java.util.ArrayList;

public class TransactionViewModel extends ViewModel {

    ArrayList<ItemLine> items ;
    ArrayList<Payment> payment ;
    String total;
    String tax;

    public void clear(){
        items.clear();
        payment.clear();
        total = new String();
        tax = new String();

    }
    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public ArrayList<ItemLine> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemLine> items) {
        this.items = items;
    }

    public ArrayList<Payment> getPayment() {
        return payment;
    }

    public void setPayment(ArrayList<Payment> payment) {
        this.payment = payment;
    }

    public  TransactionViewModel(){
        items = new ArrayList<>();
        payment = new ArrayList<>();
    }

}
