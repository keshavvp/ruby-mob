package com.example.rubymobile.data;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.rubymobile.parsers.RefreshConfigurationResponse;


public class ConfigDataViewModelFactory implements ViewModelProvider.Factory{
    Application useCase;
    static RefreshConfigData confiData;
    public ConfigDataViewModelFactory(Application useCase) {
        this.useCase = useCase;
    }
    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
//        if (modelClass.isAssignableFrom(RefreshConfigData.class)) {
        confiData =   new RefreshConfigData(useCase,new RefreshConfigurationResponse());
        return  (T) confiData;
//        } else {
//            throw new IllegalArgumentException("Unknown ViewModel class");
//        }

    }

    public static RefreshConfigData getConfiguration(){
        return confiData;
    }

}
