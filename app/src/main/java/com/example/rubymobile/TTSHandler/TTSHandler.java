package com.example.rubymobile.TTSHandler;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import java.util.Locale;

public class TTSHandler implements TextToSpeech.OnInitListener, TextToSpeech.OnUtteranceCompletedListener {
    Context con;
    static TextToSpeech textToSpeech;

    public TTSHandler(android.content.Context mainActivit) {
        con = mainActivit;

        if (textToSpeech == null) {
            textToSpeech = new TextToSpeech(mainActivit.getApplicationContext(), this);
            textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onStart(String utteranceId) {
                    Log.i("TTS", "button clicked: ");

                }

                @Override
                public void onDone(String utteranceId) {
                    Log.i("TTS", "button clicked: ");

                }

                @Override
                public void onError(String utteranceId) {
                    Log.i("TTS", "button clicked: ");

                }
            });
        }
    }


    public void speak(String data) {
        speakingDone = true;
        Log.i("TTS", "button clicked: " + data);
        int speechStatus = textToSpeech.speak(data, TextToSpeech.QUEUE_FLUSH, null, "saleID");

        while (!speakingDone){

        }
        if (speechStatus == TextToSpeech.ERROR) {
            Log.e("TTS", "Error in converting Text to Speech!");
        }
        if (speechStatus == TextToSpeech.SUCCESS) {
        }
    }

    public boolean isSpeaking() {
        return textToSpeech.isSpeaking();
    }


    public void shutdown() {
        textToSpeech.shutdown();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            textToSpeech.setOnUtteranceCompletedListener(this);
            int ttsLang = textToSpeech.setLanguage(Locale.US);
//                 Snackbar.make(,"TTS initialized",Snackbar.LENGTH_SHORT).show();
            if (ttsLang == TextToSpeech.LANG_MISSING_DATA
                    || ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "The Language is not supported!");
            } else {
                Log.i("TTS", "Language Supported.");
            }
            Log.i("TTS", "Initialization success.");
        } else {
            Log.i("TTS", "Initialization success.");

        }
    }


    @Override
    public void onUtteranceCompleted(String utteranceId) {
        Log.i("this", utteranceId); //utteranceId == "SOME MESSAGE"
        speakingDone = false;
    }

    static boolean speakingDone = false;

    public boolean isCompleted() {
        return speakingDone;
    }
}
