package com.example.rubymobile.network;

import android.util.Log;

import com.example.rubymobile.data.CommandType;
import com.example.rubymobile.data.HELPER.FileHelper;
import com.example.rubymobile.parsers.ParserFactory;
import com.saladevs.rxsse.RxSSE;
import com.saladevs.rxsse.ServerSentEvent;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;

import io.reactivex.Flowable;
import io.reactivex.subscribers.DisposableSubscriber;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class HttpConnectionUtil {

    public HttpConnectionUtil() {

    }

    static OkHttpClient client;

    public static void setOkCLient(OkHttpClient cl) {
        client = cl;
    }

    String TAG = "fdd";

    public void sendFdcPosReq(CommandType command, String token, String payload) throws Exception {
        {

            StringBuffer strBuf = new StringBuffer();
            String url = "https://192.168.31.11/cgi-bin/CGILink?";
            makeSSEListner(url, token, payload);
        }
    }

    public void makeSSEListner(String url, String token, String payload) throws URISyntaxException, IOException {
        String url1 = url + "cmd=" + CommandType.FDC.cmd + "&cookie=" + token;
        RequestBody body = RequestBody.create(
                MediaType.parse("text/xml; charset=utf-8"), payload);

        RxSSE rxsse = new RxSSE(client);
        Request request = new Request.Builder().url(url1)
                .post(body)
                .build();


        Flowable<ServerSentEvent> flowable = rxsse.connectTo(request);
        flowable.retryWhen(i -> i.flatMap(j -> Flowable.timer(3, TimeUnit.SECONDS))).subscribe(new DisposableSubscriber<ServerSentEvent>() {
            @Override
            public void onNext(ServerSentEvent serverSentEvent) {
                Log.d(TAG, "onNext" + serverSentEvent.getData());
                try {
                    ParserFactory.parseSSEEvent(serverSentEvent.getData());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable t) {
                Log.d(TAG, "onError - " + t);

            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete");

            }
        });
    }

    public void sendExtposReq(CommandType command, String token, String payload) throws Exception {
        {

            StringBuffer strBuf = new StringBuffer();
            String url = "https://192.168.31.11/cgi-bin/CGILink?";

            try {
                URL updURL = new URL(url);
                HttpsURLConnection updConnection = (HttpsURLConnection) updURL.openConnection();
                updConnection.setSSLSocketFactory(HttpsURLConnection.getDefaultSSLSocketFactory());
                updConnection.setRequestMethod("POST");
                updConnection.setDoOutput(true);
                updConnection.setDoInput(true);
                OutputStream updOStream = updConnection.getOutputStream();
                updOStream.write(("cmd=" + CommandType.C_extPos.cmd + "&cookie=" + token + "\n\n" + payload).getBytes());
                updOStream.flush();

                InputStream updIStream = (InputStream) updConnection.getContent();
                byte[] array = new byte[10240];
                int len = -1;
                do {
                    len = updIStream.read(array);
                    if (len != -1) {
                        strBuf.append(new String(Arrays.copyOfRange(array, 0, len)));
                    }
                } while (len != -1);

                System.out.println(strBuf.toString());
                try {
                    if (command != CommandType.OPEN_CASHIER) {
                        ParserFactory.parse(command, strBuf.toString());
                    }
                } catch (Exception e) {
                    throw e;
                }
                updIStream.close();
                updOStream.close();
            } catch (Exception e) {
                throw e;
            }

        }
    }

    public String sendValidateCommand(String command, String terminalId, String token) throws Exception {

        StringBuffer strBuf = new StringBuffer();
        String url = "https://192.168.31.11/cgi-bin/CGILink?";

        try {
            URL updURL = new URL(url);
            HttpsURLConnection updConnection = (HttpsURLConnection) updURL.openConnection();
            updConnection.setSSLSocketFactory(HttpsURLConnection.getDefaultSSLSocketFactory());
            updConnection.setRequestMethod("POST");
            updConnection.setDoOutput(true);
            updConnection.setDoInput(true);
            OutputStream updOStream = updConnection.getOutputStream();
            updOStream.write(("cmd=validate" + "&terminalid=" + terminalId + "&token=" + token).getBytes());
            updOStream.flush();

            InputStream updIStream = (InputStream) updConnection.getContent();
            byte[] array = new byte[10240];
            int len = -1;
            do {
                len = updIStream.read(array);
                if (len != -1) {
                    strBuf.append(new String(Arrays.copyOfRange(array, 0, len)));
                }
            } while (len != -1);
            ParserFactory.parseValidate(strBuf.toString());
            System.out.println(strBuf.toString());

            updIStream.close();
            updOStream.close();
        } catch (Exception e) {
            throw e;
        }
        return strBuf.toString();
    }

    public void sendImageReq(String imgName, String imUrl) {

        StringBuffer strBuf = new StringBuffer();
        String imageUrl = "https://192.168.31.11/" + imUrl;

        try {
            URL url = new URL(imageUrl);
            InputStream in = new BufferedInputStream(url.openStream());
            FileHelper.saveImages(in, imgName);

        } catch (Exception e) {
            Log.d("imageSync", "error in image download");
        }
//        return strBuf.toString();
    }
}
