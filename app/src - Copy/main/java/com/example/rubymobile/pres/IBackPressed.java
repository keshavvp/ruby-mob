package com.example.rubymobile.pres;

public interface IBackPressed {

    /**
     * notifies from activity when back button is pressed
     * @return
     */
    boolean backPressed();
}
