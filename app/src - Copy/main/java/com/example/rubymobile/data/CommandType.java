package com.example.rubymobile.data;

public enum CommandType {

    C_extPos("cextposreq"),POST_SALE("postFinancialTxn"),BEGIN_SESSION("beginSession"),SUSP_TXN("suspendFinancialTxn"),VOID("unpostFinancialTxn"),OPEN_CASHIER("openCashier"),CLOSE_CASHIER("closeCashier"),END_SESSION("endSession"),
    REFRESH_CONFIG("refreshConfiguration"),
    PROMPT("prompt");
    public  String cmd ;
    CommandType(String cmd){
        this.cmd =cmd;
    }


}
