package com.example.rubymobile.data;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;


public class TransactionViewModelFactory implements ViewModelProvider.Factory {

   static TransactionViewModel model;
    public  TransactionViewModelFactory(TransactionViewModel model){
        this.model = model;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
//        if (modelClass.isAssignableFrom(RefreshConfigData.class)) {
        model =   new TransactionViewModel();
        return  (T) model;
//        } else {
//            throw new IllegalArgumentException("Unknown ViewModel class");
//        }

    }
    public static TransactionViewModel getTransaction(){
        return model;
    }


}
