package com.example.rubymobile.data.HELPER;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.example.rubymobile.controller.PromptController;
import com.example.rubymobile.ent.IPromptReq;
import com.example.rubymobile.ent.PrmptCommandType;
import com.example.rubymobile.ent.PromptReqType;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;

public class FileHelper {
    final static String fileName = "refresh.txt";
//   public  final static String path = Environment.getExternalStorageDirectory() + "/Configuration/" ;
    final static String TAG = FileHelper.class.getName();

   static Context cntx;
   public static void setContext(Context ctx){
        cntx = ctx;
    }
    public static  String ReadFile( Context context){
        String line = null;

        try {

            new File(cntx.getExternalFilesDir(null),"/RubyMobile/"  );//.mkdir();

            File file = new File(cntx.getExternalFilesDir("RubyMobile"),fileName);


            FileInputStream fileInputStream = new FileInputStream (file);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();

            while ( (line = bufferedReader.readLine()) != null )
            {
                stringBuilder.append(line + System.getProperty("line.separator"));
            }
            fileInputStream.close();
            line = stringBuilder.toString();

            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            Log.d(TAG, ex.getMessage());
        }
        catch(IOException ex) {
            Log.d(TAG, ex.getMessage());
        }
        return line;
    }

    public static boolean saveToFile( String data){
        try {
            new File(cntx.getExternalFilesDir(null),"/RubyMobile/"  );//.mkdir();

            File file = new File(cntx.getExternalFilesDir("RubyMobile"),fileName);
            if (file.exists()) {
                file.delete();
             }
                file.createNewFile();

            FileOutputStream fileOutputStream = new FileOutputStream(file,true);
            fileOutputStream.write((data + System.getProperty("line.separator")).getBytes());

            return true;
        }  catch(FileNotFoundException ex) {
            Log.d(TAG, ex.getMessage());
        }  catch(IOException ex) {
            Log.d(TAG, ex.getMessage());
        }
        return  false;


    }

    public static boolean saveImages(InputStream inStream,String imageName){
        try {


            File file = new File(cntx.getExternalFilesDir("RubyMobile"),imageName);

//             File file = new File(path + "/images/" + imageName);

            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();


            OutputStream out = new BufferedOutputStream(new FileOutputStream(file));

            for ( int i; (i = inStream.read()) != -1; ) {
                out.write(i);
            }
            inStream.close();
            out.close();

            return true;
        }  catch(FileNotFoundException ex) {
            Log.d(TAG, ex.getMessage());
        }  catch(IOException ex) {
            Log.d(TAG, ex.getMessage());
        }
        return  false;


    }

    public static boolean isRefreshFilePresent(){
        return new File(cntx.getExternalFilesDir("RubyMobile"),fileName).exists();
    }
}