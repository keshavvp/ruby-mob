package com.example.rubymobile.data;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;


import com.example.rubymobile.data.HELPER.FileHelper;
import com.example.rubymobile.parsers.RefreshConfigResponseSaxParser;
import com.example.rubymobile.parsers.RefreshConfigurationResponse;

import java.io.ByteArrayInputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


public class RefreshConfigData extends AndroidViewModel {
//    RefreshConfigurationResponse res = new RefreshConfigurationResponse();
    RefreshConfigData myInstance;
    Context contxt;
    RefreshConfigurationResponse res;

    public RefreshConfigData(Application contxt,RefreshConfigurationResponse res)
    {        super(contxt);

        this.res = res;
    }

        public RefreshConfigurationResponse getConfiguration(){
            String configData = FileHelper.ReadFile(getApplication().getApplicationContext());
            try {
                SAXParserFactory factory = SAXParserFactory.newInstance();
                factory.setNamespaceAware(true);

                SAXParser saxParser = factory.newSAXParser();
                RefreshConfigResponseSaxParser refresh = new RefreshConfigResponseSaxParser(res);
                ByteArrayInputStream stream = new ByteArrayInputStream(configData.getBytes());
                saxParser.parse(stream, refresh);
             } catch (Exception e) {
                e.printStackTrace();
            }
         return res;
        }

        public void saveConfiguration(String config){
            FileHelper.saveToFile(config);

        }

}
