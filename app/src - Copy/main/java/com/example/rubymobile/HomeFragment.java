package com.example.rubymobile;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.rubymobile.TTSHandler.TTSHandler;
import com.example.rubymobile.com.example.adatper.FuelIconAdapter;
import com.example.rubymobile.com.example.adatper.FuelSaleController;
import com.example.rubymobile.data.CommandType;
import com.example.rubymobile.data.TransactionViewModelFactory;
import com.example.rubymobile.ent.PrepayItemLine;
import com.example.rubymobile.ent.TerminalDataSet;
import com.example.rubymobile.eventHandler.FuelSaleClickHandler;
import com.example.rubymobile.network.HttpConnectionUtil;
import com.example.rubymobile.pres.IBackPressed;
import com.example.rubymobile.write.Xmlwriter;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class HomeFragment extends Fragment implements IBackPressed, RecognitionListener {
    static HomeFragment myInstance;
    AlertDialog menuDialog;
    AlertDialog inputMenuDialg;
    TTSHandler ttsHAndler;
     SpeechRecognizer rec;
    FuelSaleController fuelCtrl;

    public static HomeFragment getInstance() {
        if (myInstance == null) {
            myInstance = new HomeFragment();
        }
        return myInstance;
    }

    public void setTTS(TTSHandler ttsHAndler) {
        this.ttsHAndler = ttsHAndler;

    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        return inflater.inflate(R.layout.fragment_home, container, false);

    }

    @Override
    public void onStart() {
        fuelCtrl = new FuelSaleController(getContext());
        super.onStart();

        GridView gridView = (GridView) getView().findViewById(R.id.iconGrid);
        gridView.setAdapter(new FuelIconAdapter(this.getActivity()));
        final AlertDialog.Builder menuItem = new AlertDialog.Builder(getContext());
        final AlertDialog.Builder inputMenItem = new AlertDialog.Builder(getContext());
        gridView.setOnItemClickListener(new FuelSaleClickHandler(getContext(), fuelCtrl, menuDialog, inputMenuDialg));
        /*gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                View fuelMenu = inflater.inflate(R.layout.custom_menu_items_view, null);
                TextView header = fuelMenu.findViewById(R.id.header);
                header.setText("Fueling Position "+ (position + 1));

                Button btn1 =  fuelMenu.findViewById(R.id.option1);
                btn1.setVisibility(View.VISIBLE);
                btn1.setText("Prepay");
                btn1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        menuDialog.dismiss();
                        LayoutInflater inflater = LayoutInflater.from(getContext());
                        View inputMenu = inflater.inflate(R.layout.custom_input_menu_items, null);
                        TextView header = inputMenu.findViewById(R.id.header);
                        header.setText("Fuel Parameters");
                        TextView lfpNum =  inputMenu.findViewById(R.id.label1);
                        lfpNum.setText("Enter Position Number");
                        TextView fpNum =  inputMenu.findViewById(R.id.inputText1);
                        fpNum.setText(String.valueOf(position + 1));
                        fpNum.setClickable(false);

                        fpNum.setInputType(InputType.TYPE_NULL);

                        TextView lAmt =  inputMenu.findViewById(R.id.label2);
                        lAmt.setText("Enter Amount");
                        TextView amt =  inputMenu.findViewById(R.id.inputText2);
                        amt.setInputType((InputType.TYPE_CLASS_NUMBER + InputType.TYPE_NUMBER_FLAG_DECIMAL));
                        lAmt.setVisibility(View.VISIBLE);
                        TextView amT =  inputMenu.findViewById(R.id.inputText2);
                        amT.setVisibility(View.VISIBLE);
                        Button cancel =  inputMenu.findViewById(R.id.cancel);
                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                inputMenuDialg.dismiss();
                            }
                        });
                        inputMenItem.setView(inputMenu);
                       inputMenuDialg = inputMenItem.create();inputMenuDialg.show();
                    }
                });

                Button btn2 =  fuelMenu.findViewById(R.id.option2);
                btn2.setVisibility(View.VISIBLE);
                btn2.setText("Postpay");

                Button btn3 =  fuelMenu.findViewById(R.id.option3);
                btn3.setVisibility(View.VISIBLE);
                btn3.setText("Preset");

                menuItem.setView(fuelMenu);
                menuDialog = menuItem.create();
                menuDialog.show();

            }
        });*/
        Button merchandise = getView().findViewById(R.id.merchandise);
        merchandise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).moveToMerchandise();
            }
        });

        Button fuel = getView().findViewById(R.id.fuelSales);
        fuel.setOnClickListener(new FuelSaleClickHandler(getContext(), fuelCtrl, menuDialog, inputMenuDialg));

        Button utility = getView().findViewById(R.id.utilityFunctions);
        utility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                View fuelMenu = inflater.inflate(R.layout.custom_menu_items_view, null);
                Button btn1 = fuelMenu.findViewById(R.id.option1);
                TextView header = fuelMenu.findViewById(R.id.header);
                header.setText("Select Function");
                btn1.setVisibility(View.VISIBLE);
                btn1.setText("Send Logs");

               /* Button btn2 =  fuelMenu.findViewById(R.id.option2);
                btn2.setVisibility(View.VISIBLE);
                btn2.setText("POST PAY");

                Button btn3 =  fuelMenu.findViewById(R.id.option3);
                btn3.setVisibility(View.VISIBLE);
                btn3.setText("PRESET");*/

                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setView(fuelMenu);
                AlertDialog dialog = alert.create();
                alert.show();

            }
        });

        Button cashierFunction = getView().findViewById(R.id.cashierFunctions);
        cashierFunction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                View fuelMenu = inflater.inflate(R.layout.custom_menu_items_view, null);
                Button btn1 = fuelMenu.findViewById(R.id.option1);
                TextView header = fuelMenu.findViewById(R.id.header);
                header.setText("Select Function");
                btn1.setVisibility(View.VISIBLE);
                btn1.setText("Open Cashier");

                Button btn2 = fuelMenu.findViewById(R.id.option2);
                btn2.setVisibility(View.VISIBLE);
                btn2.setText("Refresh Configuration");
                btn2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        menuDialog.dismiss();
                        showSnakBar("refreshing the configuration");
                        sendRefreshConfig();

                    }
                });

                Button btn3 = fuelMenu.findViewById(R.id.option3);
                btn3.setVisibility(View.VISIBLE);
                btn3.setText("Close Cashier");

                Button btn4 = fuelMenu.findViewById(R.id.option4);
                btn4.setVisibility(View.VISIBLE);
                btn4.setText("Log out");
                btn4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendEndSession();
                        menuDialog.dismiss();
                        TransactionViewModelFactory.getTransaction().clear();
                        getActivity().finish();

                    }
                });

              /* menuDialog = alert.create();
                menuDialog.create();
               menuItem.show();*/


                menuItem.setView(fuelMenu);
                menuDialog = menuItem.create();
                menuDialog.show();

            }
        });
//        startListning();
//         createSR();



        ImageView btn = getView().findViewById(R.id.voice);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//speech input

                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
     /*           intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
                intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getContext().getPackageName());*/
                startActivityForResult(intent,1);
               /* Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);


                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
                intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getContext().getPackageName());
                intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        intent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true);
                    }
                rec.startListening(intent);*/

            }

        });

    }
    Intent intent;
    public void startListning() {
          intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 2000); // value to wait
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, 2000);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getContext().getPackageName());
    }

    public void createSR() {
        if(rec != null){
            rec.destroy();

        }
        rec = SpeechRecognizer.createSpeechRecognizer(this.getContext());

        if (SpeechRecognizer.isRecognitionAvailable(getContext())) {
            rec.setRecognitionListener(this);
            rec.setRecognitionListener(this);
        } else {
            // Handle error
        }

        rec.startListening(intent);

    }

    public void showSnakBar(String messages) {

        Snackbar.make(getView(), messages, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ArrayList<String> list = new ArrayList<>();
        try {
            list = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            Toast.makeText(getContext(), "speech !" + list.get(0), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
        }
        if (resultCode == -1 && data != null) {

            switch (requestCode) {
                case 10:

                case 20:

                case 30:

            }
        } else {
            Toast.makeText(getContext(), "Failed to recognize speech!", Toast.LENGTH_LONG).show();
        }
        performVoiceCommandAction(list);
    }

    private void performVoiceCommandAction(ArrayList<String> asd) {

          if ((asd.get(0).equalsIgnoreCase("OK verifone")) ||
                (asd.get(0).equalsIgnoreCase("OK veri phone")) ||
                (asd.get(0).equalsIgnoreCase("OK very Phone")) ||
                (asd.get(0).equalsIgnoreCase("OK very form")) ||
                (asd.get(0).equalsIgnoreCase("OK veryphone")) ){

            ttsHAndler.speak("Hello, Good morning. " + "How can i help you");


        }


            if (asd != null && !asd.isEmpty() &&
                    ((asd.get(0).equalsIgnoreCase("refresh config")) ||
                            (asd.get(0).equalsIgnoreCase("")) ||
                            (asd.get(0).equalsIgnoreCase("sync with commander")))) {
                sendRefreshConfig();
                this.ttsHAndler.speak("refreshing the configurations");
            } else if (asd != null && !asd.isEmpty()) {
                String command = asd.get(0);
//            String command = "add $10.00 prepay to fueling position 3";
                Pattern p = Pattern.compile("\\$[0-9]*(\\.[0-9]{2})?");


// 	                 Pattern p  = Pattern.compile("(?:[$])\\s*\\d+(?:\\.\\d{2})?");
                boolean matcher = Pattern.matches("^\\$[0-9]+(\\.[0-9]{1,2})?[ a-z]+[1-9]+$", command);
//	                 String s = matcher.grounp();
                if (matcher) {
                    Matcher mat = p.matcher(command);
                    String amt = "";
                    if (mat.find()) {
                        amt = mat.group();
                    }

                    Pattern p1 = Pattern.compile("[a-z]+[ ][0-9]+");
                    Matcher mat1 = p1.matcher(command);
                    String fPos = "";
                    if (mat1.find()) {
                        fPos = mat1.group();
                    }
               /* if(fPos.startsWith("num")){
                    fPos = "position " +fPos;
                }*/
                    if (!amt.equals("")) {
                        Toast.makeText(getContext(), "fPos " + fPos + " amt" + amt, Toast.LENGTH_SHORT);
                        this.ttsHAndler.speak("Adding prepay of" + amt + " to the" + fPos);
                    }


                    PrepayItemLine fuelItm = new PrepayItemLine();
                    String[] fp = fPos.split(" ");
                    fuelItm.setDisplayName("Prepay    #" + (fp[1]));
                    String sym = String.valueOf(amt.charAt(0));
                    fuelItm.setSym(sym);
                    fuelItm.setFuelingPosition(fp[1]);
                    fuelItm.setAmount(amt.substring(1));
//                TransactionViewModelFactory.getTransaction().getItems().add(fuelItm);
                    fuelCtrl.addFuelItemLine(fuelItm);

                }

            }
        if (asd != null && !asd.isEmpty() &&
                ((asd.get(0).equalsIgnoreCase("End Session")) ||
                        (asd.get(0).equalsIgnoreCase("Log Out")) ||
                        (asd.get(0).equalsIgnoreCase("LogOut")))) {
            sendEndSession();

            this.ttsHAndler.speak("Logging Out");
            TransactionViewModelFactory.getTransaction().clear();
            getActivity().finish();
        }

      }

    private void sendEndSession() {
        Runnable run = () ->
        {
            HttpConnectionUtil con = new HttpConnectionUtil();
            try {
                con.sendExtposReq(CommandType.END_SESSION, TerminalDataSet.getCookie(), Xmlwriter.getendSessionPayload());
            } catch (Exception e) {
            }

        };
        new Thread(run).start();
    }

    private void sendRefreshConfig() {
        Runnable run = () ->
        {
            HttpConnectionUtil con = new HttpConnectionUtil();
            try {
                con.sendExtposReq(CommandType.REFRESH_CONFIG, TerminalDataSet.getCookie(), Xmlwriter.getRefreshConfigXmlPayload());
            } catch (Exception e) {
            }

        };
        new Thread(run).start();
    }

    public void speak(String speak) {
        ttsHAndler.speak(speak);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean backPressed() {
        boolean callSuper = true;

        return callSuper;
    }

    @Override
    public void onReadyForSpeech(Bundle params) {

    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onRmsChanged(float rmsdB) {

    }

    @Override
    public void onBufferReceived(byte[] buffer) {

    }

    @Override
    public void onEndOfSpeech() {
        rec.stopListening();
    }

    @Override
    public void onError(int error) {
//        showSnakBar("onError");
        rec.stopListening();
        rec.destroy();
      }

    @Override
    public void onResults(Bundle results) {
        rec.destroy();
        ArrayList<String> result = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        performVoiceCommandAction(result);
        showSnakBar("onResults " + result.get(0));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (ttsHAndler.isCompleted()){

        }
//       createSR();


    }

    @Override
    public void onPartialResults(Bundle partialResults) {

    }

    @Override
    public void onEvent(int eventType, Bundle params) {

    }


}