package com.example.rubymobile.websocket;

import android.util.Log;


import com.example.rubymobile.controller.PromptController;
import com.example.rubymobile.data.CommandType;
import com.example.rubymobile.ent.IPromptReq;
import com.example.rubymobile.parsers.ParserFactory;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;

public class WsClient extends WebSocketClient {

        public WsClient( URI serverUri ) {
            super( serverUri );
        }

        @Override
        public void onOpen( ServerHandshake handshakedata ) {
            System.out.println( "Connected" );

        }

        @Override
        public void onMessage( String message ) {
            System.out.println( "got: " + message );
            if(WsUtil.isPostFinTxnResponse(message)){
               try{
                    ParserFactory.parse(CommandType.POST_SALE,message);

               }catch (Exception e){
                   Log.d("post sae resp","cant handle");
               }
            }
            else if(WsUtil.isCancelPrompt(message)){
                 PromptController.clear();
            }
            else{
               try {
                   IPromptReq req=  ParserFactory.createPromptReq(message);
//                 PromptController.handlePorompt(req);
               }catch (Exception e){
                   Log.d("prompt req","cant handle");
               };

            }


        }

        @Override
        public void onClose( int code, String reason, boolean remote ) {
            System.out.println( "Disconnected" );

        }

        @Override
        public void onError( Exception ex ) {
            ex.printStackTrace();

        }

    }

