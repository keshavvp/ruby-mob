package com.example.rubymobile.ent;

public interface ItemLine {
    public String getDisplayName();

    public void setDisplayName(String displayName);

    public String getDateModified();

    public void setDateModified(String dateModified);

    public String getIconName();


    public void setIconName(String iconName);

    public String getIconUrl();

    public void setIconUrl(String iconUrl);

    public boolean isHotItem();

    public void setHotItem(boolean hotItem);

    public int getQty() ;
    public void setQty(int qty) ;
    public String getUPC();
    public void setUPC(String uPC);
    public String getModifier();
    public void setModifier(String modifier);

    /**
     * only for prepay
     *
     */
    public String getAmount() ;
    public void setAmount(String amount) ;
    public String getSym() ;
    public void setSym(String sym) ;


}
