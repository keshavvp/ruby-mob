package com.example.rubymobile.ent;

import java.util.ArrayList;
import java.util.List;

public class DisplayMessageType implements IPromptReq {
	long promptId;
	int timeOut;
	private String header;
	private PrmptCommandType cmd;
	private List<String> msg = new ArrayList<String>();
	private boolean isCancellable;
	@Override
	public void setPromptID(long promptId) {
		  this.promptId =promptId;
		
	}

	@Override
	public void settimeOut(int timeOut) {
		 this.timeOut =timeOut;
		
	}

	@Override
	public void setHeader(String header) {
		this.header=header;
		
	}

	@Override
	public void setCommandType(PrmptCommandType cmd) {
		 this.cmd = cmd;
		
	}

	@Override
	public void setMessage(List<String> msg) {
		this.msg= msg;
		
	}

	@Override
	public void setIsCancellable(boolean cancellable) {
		this.isCancellable = cancellable;
		
	}

	@Override
	public long getPromptId() {
		 
		return promptId;
	}

	@Override
	public int getTimeOut() {
		return timeOut;
	}

	@Override
	public String getHeader() {
		return header;
	}

	@Override
	public PrmptCommandType getCmd() {
		return cmd;
	}

	@Override
	public List<String> getMsg() {
		return msg;
	}

	@Override
	public boolean isCancellable() {
		return isCancellable;
	}

}
