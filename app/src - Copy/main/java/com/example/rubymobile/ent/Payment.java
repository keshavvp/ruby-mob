package com.example.rubymobile.ent;

import java.math.BigDecimal;

/**
 * 
 * @author T_VaishnaviS1
 *
 */
public class Payment{
	
	private String sysid;
	private String isCardBased;
	private String mopCode;
	private String name;
	private BigDecimal amount;
	private boolean applied = false;
	public boolean isApplied() {
		return applied;
	}
	public void setApplied(boolean applied) {
		this.applied = applied;
	}
	public String getSysid() {
		return sysid;
	}
	public void setSysid(String sysid) {
		this.sysid = sysid;
	}
	public String isCardBased() {
		return isCardBased;
	}
	public void setCardBased(String isCardBased) {
		this.isCardBased = isCardBased;
	}
	public String getMopCode() {
		return mopCode;
	}
	public void setMopCode(String mopCode) {
		this.mopCode = mopCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
