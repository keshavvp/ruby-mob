package com.example.rubymobile.ent;

public class PrepayItemLine implements ItemLine {

    private String displayName;
    private String amount;
    private String fuelingPosition;
    private String sym;

    public String getFuelingPosition() {
        return fuelingPosition;
    }

    public void setFuelingPosition(String fuelingPosition) {
        this.fuelingPosition = fuelingPosition;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAmount() {
        return amount;
    }

    @Override
    public String getSym() {
        return sym;
    }

    @Override
    public void setSym(String sym) {
        this.sym = sym;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String getDateModified() {
        return null;
    }

    @Override
    public void setDateModified(String dateModified) {

    }

    @Override
    public String getIconName() {
        return null;
    }

    @Override
    public void setIconName(String iconName) {

    }

    @Override
    public String getIconUrl() {
        return null;
    }

    @Override
    public void setIconUrl(String iconUrl) {

    }

    @Override
    public boolean isHotItem() {
        return false;
    }

    @Override
    public void setHotItem(boolean hotItem) {

    }

    @Override
    public int getQty() {
        return 0;
    }

    @Override
    public void setQty(int qty) {

    }

    @Override
    public String getUPC() {
        return null;
    }

    @Override
    public void setUPC(String uPC) {

    }

    @Override
    public String getModifier() {
        return null;
    }

    @Override
    public void setModifier(String modifier) {

    }
}
