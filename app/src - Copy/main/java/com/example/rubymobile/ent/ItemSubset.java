package com.example.rubymobile.ent;

import java.util.ArrayList;

public class ItemSubset {

	private String name;
 
 	ArrayList<ItemSubSetItem> subSetItems = new ArrayList<>();

 

	public static final int ITEMSET_NAME_LENGTH = 12; 
	public static final int MAX_ITEMSET_ID = 99;
	public static final int MAX_COLOR_CODE = 7;
	public static final String [] COLOR_CODES = new String[] {"#2D8F63", "#607FB2", "#AA3939", "#AA5C39", "#AA7539", "#865131", "#666666"};

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<ItemSubSetItem> getsubSetItems() {
		return subSetItems;
	}
	public void setsubSetItems(ArrayList<ItemSubSetItem> items) {
		this.subSetItems = items;
	}
	 
	public static String[] getColorCodes() {
		return COLOR_CODES;
	}
		

}
