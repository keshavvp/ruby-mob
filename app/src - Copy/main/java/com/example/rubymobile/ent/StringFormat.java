package com.example.rubymobile.ent;

public class StringFormat {
	
	public static final int LEFT_ALIGN            = 1;
	public static final int RIGHT_ALIGN           = 2;
	public static final int CENTER_ALIGN          = 3;
	public static final int MAX_LINE_SIZE         = 40;

    /**
     * Creates a left justified string with the specified size and
     * alignment (left default).
     * 
	 * same size as the input String, the input string is returned.
     * shorter than the input String, the input String is truncated. 
     * longer than the input String, the input String is padded
     * with spaces. This will always return a string of 40 character.
	 * 
     * @param strs	The array of input String to be justified
     * @param size	The array size of the output String
     * @return
     */
    public static String formatLine(String strs[], int size[]){
    	
    	StringBuffer buffer = new StringBuffer();
    	for(int index = 0; index < strs.length; index++){
    		buffer.append(formatLeft(strs[index], size[index]));
    	}
    	return formatLeft(buffer.toString(), MAX_LINE_SIZE);
    }
    
    /**
     * Creates a multiple justified string with the specified size and
     * alignment.
     * 
	 * same size as the input String, the input string is returned.
     * shorter than the input String, the input String is truncated. 
     * longer than the input String, the input String is padded
     * with spaces. This will always return a string of 40 character.
	 * 
     * @param strs	The array of input String to be justified
     * @param size	The array size of the output String
     * @param alignment The array alignment of the output String
     * @return
     */
    public static String formatLine(String strs[], int size[], int alignment[]){
    	
    	StringBuffer buffer = new StringBuffer();
    	for(int index = 0; index < strs.length; index++){
    		if(alignment[index] == LEFT_ALIGN){
    			buffer.append(formatLeft(strs[index], size[index]));
    		}else if(alignment[index] == RIGHT_ALIGN){
    			buffer.append(formatRight(strs[index], size[index]));
    		}else{
    			buffer.append(formatCenter(strs[index], size[index]));
    		}
    	}
    	return formatLeft(buffer.toString(), MAX_LINE_SIZE);
    }
    
    /**
	 * Creates a right justified string with the specified size.  If the 
	 * specified size is:
	 * 
	 * same size as the input String, the input string is returned.
     * shorter than the input String, the input String is truncated. 
     * longer than the input String, the input String is left padded
     * with spaces.
	 * 
	 * @param size  The size of the output String
	 * @param value The input String to be right justified
	 * @return A string that is right justified and left padded with spaces
	 */
    public static String formatRight(String inputString, int size){
		String s = null;
		if (inputString == null){
			s = createBlankString(size);
		}else if (inputString.length() < size){
			StringBuffer sb = new StringBuffer(size);
			sb.append(createBlankString(size - inputString.length()));
			sb.append(inputString);
			s = sb.toString();
		}else if (inputString.length() > size){
			s = inputString.substring(inputString.length() - size, inputString.length()); 
		}else{
			s = inputString;
		}
		return s;
	}
    
    /**
	 * Creates a left justified string with the specified size.  If the 
	 * specified size is:
	 * 
	 * same size as the input String, the input string is returned.
     * shorter than the input String, the input String is truncated. 
     * longer than the input String, the input String is right padded
     * with spaces.
	 * 
	 * @param size  The size of the output String
	 * @param value The input String to be left justified
	 * @return A string that is left justified and padded with spaces
	 */
    public static String formatLeft(String inputString, int size){
		String s = null;
		if (inputString == null){
			/** if the input is null, return a string of spaces **/
			s = createBlankString(size);
		}else if (inputString.length() < size){
			StringBuffer sb = new StringBuffer(size);
			sb.append(inputString);
			sb.append(createBlankString((size - inputString.length())));
			s = sb.toString();
		}else if (inputString.length() > size){
			s = inputString.substring(0, size);
		}else {  /** input string length must be same as specified size **/
			s = inputString;
		}
		return s;
	}
    
    /**
	 * Creates a left justified string with the specified size.  If the 
	 * specified size is:
	 * 
	 * same size as the input String, the input string is returned.
     * shorter than the input String, the input String is truncated. 
     * longer than the input String, the input String is right padded
     * with spaces.
	 * 
	 * @param size  The size of the output String
	 * @param value The input String to be left justified
	 * @return A string that is left justified and padded with spaces
	 */
    public static String formatCenter(String inputString, int size){
		String s = null;
		if (inputString == null){
			/** if the input is null, return a string of spaces **/
			s = createBlankString(size);
		}else if (inputString.length() < size){
			StringBuffer sb = new StringBuffer(size);
			int blankSize = size - inputString.length();
			int quotient = blankSize / 2;
			int remainder = blankSize % 2;
			sb.append(createBlankString(quotient));
			sb.append(inputString);
			sb.append(createBlankString(quotient + remainder));
			s = sb.toString();
		}else if (inputString.length() > size){
			s = inputString.substring(0, size);
		}else { /** input string length must be same as specified size **/
			s = inputString;
		}
		return s;
	}
    
    /**
	 * Creates a string containing the space character
	 * with the specified length.
	 * 
	 * @return String that contains the specified character
	 * @param   size  length of string
	 * @param   fillerChar the character used to fill this string
	 */
    public static String createBlankString(int size){
		return createString(size, ' ');
	}
    
    /**
	 * Creates a string containing the specified character
	 * with the specified length.
	 * 
	 * @return String that contains the specified character
	 * @param   size  length of string
	 * @param   fillerChar the character used to fill this string
	 */
    public static String createString(int size, char fillerChar){
		StringBuffer sb = new StringBuffer(size);
		for (int i = 0; i < size; i++){
			sb.append(fillerChar);
		}
		return sb.toString();
	}


}
