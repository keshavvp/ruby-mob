package com.example.rubymobile.ent;

import java.math.BigDecimal;


public class Item {
	
	private ITEM_TYPE itemType;
	private EXCEPTION_TYPE exceptionType;
	//PLU
	private String UPC;
	private String modifier;
	private String name;
	private String currency;
	private String symbol;
	
	private boolean triggerPostFinancialTxn;
	
	//PREPAY
	private int fuelingPosition;
	private BigDecimal amountLImit;
	private int grade;
	
	
	private BigDecimal quantity;
	private BigDecimal amount;
	
	//Exception
	private boolean customerDOB;
	private boolean promptFlag = false;	
	private String promptType;
	private String promptScope;
	private String promptHeader;
	private String promptField;

	enum ITEM_TYPE {
		PLU, PREPAY, FUEL;
	}
	
	enum EXCEPTION_TYPE{
		itemNotAllowed, itemNotFound, itemInvalid, customerDOB
	}

	public boolean isFuelItem(){
		return itemType == ITEM_TYPE.FUEL;
	}
	
	public boolean isPrepayItem() {
		return itemType == ITEM_TYPE.PREPAY;
	}

	public boolean isPluItem(){
		return itemType == ITEM_TYPE.PLU;
	}
	
	public void setPrepayItemType(){
		itemType = ITEM_TYPE.PREPAY;
	}
	
	public void setFuelItemType(){
		itemType = ITEM_TYPE.FUEL;
	}
	
	public void setPluItemType(){
		itemType = ITEM_TYPE.PLU;
	}
	
	public boolean isItemNotAllowed() {
		return exceptionType == EXCEPTION_TYPE.itemNotAllowed;
	}
	
	public boolean isItemNotFound() {
		return exceptionType == EXCEPTION_TYPE.itemNotFound;
	}
	
	public boolean isItemInvalid() {
		return exceptionType == EXCEPTION_TYPE.itemInvalid;
	}
	
	public boolean isCustomerDOB() {
		return exceptionType == EXCEPTION_TYPE.customerDOB;
	}
	
	public void setItemNotAllowed() {
		exceptionType = EXCEPTION_TYPE.itemNotAllowed;
	}
	
	public void setItemNotFound() {
		exceptionType = EXCEPTION_TYPE.itemNotFound;
	}
	
	public void setItemInvalid() {
		exceptionType = EXCEPTION_TYPE.itemInvalid;
	}
	
	public void setCustomerDOB() {
		exceptionType = EXCEPTION_TYPE.customerDOB;
	}
	/**
	 * @return the uPC
	 */
	public String getUPC() {
		return UPC;
	}

	/**
	 * @param uPC the uPC to set
	 */
	public void setUPC(String uPC) {
		UPC = uPC;
	}

	/**
	 * @return the modifier
	 */
	public String getModifier() {
		return modifier;
	}

	/**
	 * @param modifier the modifier to set
	 */
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the fuelingPosition
	 */
	public int getFuelingPosition() {
		return fuelingPosition;
	}

	/**
	 * @param fuelingPosition the fuelingPosition to set
	 */
	public void setFuelingPosition(int fuelingPosition) {
		this.fuelingPosition = fuelingPosition;
	}

	/**
	 * @return the amountLImit
	 */
	public BigDecimal getAmountLImit() {
		return amountLImit;
	}

	/**
	 * @param amountLImit the amountLImit to set
	 */
	public void setAmountLImit(BigDecimal amountLImit) {
		this.amountLImit = amountLImit;
	}

	/**
	 * @return the grade
	 */
	public int getGrade() {
		return grade;
	}

	/**
	 * @param grade the grade to set
	 */
	public void setGrade(int grade) {
		this.grade = grade;
	}
	/**
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the customerDOB
	 */
//	public boolean isCustomerDOB() {
//		return customerDOB;
//	}
//
//	/**
//	 * @param customerDOB the customerDOB to set
//	 */
//	public void setCustomerDOB(boolean customerDOB) {
//		this.customerDOB = customerDOB;
//	}
	
	public boolean isPrompt() {
		return promptFlag;
	}
	
	public void setPromptFlag(boolean promptFlag) {
		this.promptFlag = promptFlag;
	}
	public String getPromptType() {
		return promptType;
	}
	public void setPromptType(String promptType) {
		this.promptType = promptType;
	}
	public String getPromptScope() {
		return promptScope;
	}
	public void setPromptScope(String promptScope) {
		this.promptScope = promptScope;
	}
	public String getPromptHeader() {
		return promptHeader;
	}
	public void setPromptHeader(String promptHeader) {
		this.promptHeader = promptHeader;
	}
	public String getPromptField() {
		return promptField;
	}
	public void setPromptField(String promptField) {
		this.promptField = promptField;
	}
	

	public boolean isTriggerPostFinancialTxn() {
		return triggerPostFinancialTxn;
	}

	public void setTriggerPostFinancialTxn(boolean triggerPostFinancialTxn) {
		this.triggerPostFinancialTxn = triggerPostFinancialTxn;
	}
	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getcurrency() {
		return currency;
	}

	public void setcurrency(String currency) {
		this.currency = currency;
	}
}

