package com.example.rubymobile.ent;

import java.util.List;

public interface IPromptReq {
	
	public void setPromptID(long promptId);
	public void settimeOut(int timeOut);
	public void setHeader(String header);
	public void setCommandType(PrmptCommandType cmd);
	public void setMessage(List<String> message);
	public void setIsCancellable(boolean cancellable);
	public long getPromptId();
	public int getTimeOut() ;
	public String getHeader();
	public PrmptCommandType getCmd();
	public List<String> getMsg();
	public boolean isCancellable();
	
}
