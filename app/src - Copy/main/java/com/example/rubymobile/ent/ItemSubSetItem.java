package com.example.rubymobile.ent;

public class ItemSubSetItem implements ItemLine{

	
	private String displayName;
	private String dateModified;
	private String iconName;
	private String iconUrl;
	private String UPC;
	private String modifier;
	private int qty;
	private boolean hotItem;
    
    public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDateModified() {
		return dateModified;
	}

	public void setDateModified(String dateModified) {
		this.dateModified = dateModified;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public boolean isHotItem() {
		return hotItem;
	}

	public void setHotItem(boolean hotItem) {
		this.hotItem = hotItem;
	}

	@Override
	public int getQty() {
		return qty;
	}

	@Override
	public void setQty(int qty) {
		this.qty =qty;
	}


	public String getUPC() {
		return UPC;
	}
	public void setUPC(String uPC) {
		UPC = uPC;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	@Override
	public String getAmount() {
		return null;
	}

	@Override
	public void setAmount(String amount) {

	}

	@Override
	public String getSym() {
		return null;
	}

	@Override
	public void setSym(String sym) {

	}

}