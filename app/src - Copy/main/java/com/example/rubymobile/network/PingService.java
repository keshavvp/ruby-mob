package com.example.rubymobile.network;

import android.util.Log;


import com.example.rubymobile.ent.PromptReqType;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class PingService {

    public static boolean isStop = false;
    public static boolean isReachable = false;

    public static void start() {
        Runnable pingService = () -> {
         while (!isStop){  try {
                sendPingRequest("192.168.31.11");

            } catch (Exception e) {
            }
         }
        }; new Thread(pingService).start();
    }
    public static void stop() {
        isStop =true;
    }
    private static void sendPingRequest(String ipAddress)
            throws UnknownHostException, IOException
    {
        InetAddress geek = InetAddress.getByName(ipAddress);
        System.out.println("Sending Ping Request to " + ipAddress);
        if (geek.isReachable(10000)){
            isReachable = true;
            Log.d("PingService","Host is reachable");
        }
        else{
            isReachable = false;

            PromptReqType req = new PromptReqType();
            req.setHeader("Unable to reach server");
            req.getMsg().add("Out of Service");
            req.settimeOut(0);
//            PromptController.handlePorompt(req);
            Log.d("PingService","Sorry ! We can't reach to this host");
        }
    }
}
