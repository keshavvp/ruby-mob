package com.example.rubymobile.network;

import android.util.Log;

import com.example.rubymobile.data.CommandType;
import com.example.rubymobile.data.HELPER.FileHelper;
import com.example.rubymobile.parsers.ParserFactory;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Arrays;

import javax.net.ssl.HttpsURLConnection;

public class HttpConnectionUtil {

    public HttpConnectionUtil(){

    }
    public void sendExtposReq(CommandType command, String token, String payload) throws Exception{
        {

            StringBuffer strBuf = new StringBuffer();
            String url = "https://192.168.31.11/cgi-bin/CGILink?";

            try {
                URL updURL = new URL(url);
                HttpsURLConnection updConnection = (HttpsURLConnection)updURL.openConnection();
                updConnection.setSSLSocketFactory(HttpsURLConnection.getDefaultSSLSocketFactory());
                updConnection.setRequestMethod("POST");
                updConnection.setDoOutput(true);
                updConnection.setDoInput(true);
                OutputStream updOStream = updConnection.getOutputStream();
                updOStream.write(("cmd="+CommandType.C_extPos.cmd+ "&cookie=" + token +"\n\n"+ payload).getBytes());
                updOStream.flush();

                InputStream updIStream = (InputStream) updConnection.getContent();
                byte[] array = new byte[10240];
                int len = -1;
                do{
                    len = updIStream.read(array);
                    if(len != -1){
                        strBuf.append(new String(Arrays.copyOfRange(array, 0, len)));
                    }
                } while(len != -1);

                System.out.println(strBuf.toString());
              try{
                  if(command != CommandType.OPEN_CASHIER) {
                      ParserFactory.parse(command, strBuf.toString());
                  }
              }catch (Exception e) {
                  throw e;
              }
                updIStream.close();
                updOStream.close();
            } catch (Exception e) {
                throw e;
             }

        }
    }
    public String sendValidateCommand (String command, String terminalId, String token) throws Exception {

        StringBuffer strBuf = new StringBuffer();
        String url = "https://192.168.31.11/cgi-bin/CGILink?";

        try {
            URL updURL = new URL(url);
            HttpsURLConnection updConnection = (HttpsURLConnection)updURL.openConnection();
            updConnection.setSSLSocketFactory(HttpsURLConnection.getDefaultSSLSocketFactory());
            updConnection.setRequestMethod("POST");
            updConnection.setDoOutput(true);
            updConnection.setDoInput(true);
            OutputStream updOStream = updConnection.getOutputStream();
            updOStream.write(("cmd=validate" +"&terminalid=" + terminalId +"&token=" + token).getBytes());
            updOStream.flush();

            InputStream updIStream = (InputStream) updConnection.getContent();
            byte[] array = new byte[10240];
            int len = -1;
            do{
                len = updIStream.read(array);
                if(len != -1){
                    strBuf.append(new String(Arrays.copyOfRange(array, 0, len)));
                }
            } while(len != -1);
            ParserFactory.parseValidate(strBuf.toString());
            System.out.println(strBuf.toString());

            updIStream.close();
            updOStream.close();
        } catch (Exception e) {
            throw e;
        }
        return strBuf.toString();
    }

    public void sendImageReq(String imgName, String imUrl) {

        StringBuffer strBuf = new StringBuffer();
        String imageUrl = "https://192.168.31.11/"+imUrl;

        try {
             URL url = new URL(imageUrl);
            InputStream in = new BufferedInputStream(url.openStream());
             FileHelper.saveImages(in,imgName);

        } catch (Exception e) {
            Log.d("imageSync","error in image download");
        }
//        return strBuf.toString();
    }
}
