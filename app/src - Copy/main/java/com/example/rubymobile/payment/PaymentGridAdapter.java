package com.example.rubymobile.payment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;

import com.example.rubymobile.R;
import com.example.rubymobile.data.ConfigDataViewModelFactory;
import com.example.rubymobile.data.RefreshConfigData;
import com.example.rubymobile.ent.Payment;

import java.math.BigDecimal;
import java.util.ArrayList;

public class PaymentGridAdapter extends BaseAdapter {
    RefreshConfigData dataModel;
    GridView payGrid ;
    Context payActivity;
    ArrayList<Payment> mopList;
    public PaymentGridAdapter(GridView payGrid , Context payActivity) {
        dataModel = ConfigDataViewModelFactory.getConfiguration();
        this.payGrid= payGrid;
        this.payActivity = payActivity;
        mopList =mopList = dataModel.getConfiguration().getMopList();;
      /*  Payment cash = new Payment();
        cash.setAmount(BigDecimal.TEN);
        cash.setName("CASH");
        mopList.add(cash);
        Payment credit = new Payment();
        credit.setAmount(BigDecimal.TEN);
        credit.setName("CREDIT");
        mopList.add(credit);
        Payment debit = new Payment();
        debit.setAmount(BigDecimal.TEN);
        debit.setName("DEBIT");
        mopList.add(debit);*/
       for(Payment mop : mopList){
          /* if(mop.getName().equalsIgnoreCase("CASH")){
               cash = mop;
           }*/
       }/*if(cash !=null){
           mopList.remove(cash);
        }*/
    }

        @Override
    public int getCount() {
        return mopList.size();
    }

    @Override
    public Object getItem(int position) {
        return mopList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) payActivity.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        View view = inflater.inflate(R.layout.payment_grid_items,null);
        TextView mopName = view.findViewById(R.id.mopName);
       ImageView img = view.findViewById(R.id.mopimageView);
        mopName.setText(mopList.get(position).getName());
        if(mopList.get(position).getName().equals("CASH")){
            img.setImageResource(R.drawable.cash);
        }else{
            img.setImageResource(R.drawable.networkpay);
        }

//        view.setBackgroundColor(Color.WHITE);
        return view;
    }
}
