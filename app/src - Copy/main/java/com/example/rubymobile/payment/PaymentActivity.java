package com.example.rubymobile.payment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.rubymobile.R;
import com.example.rubymobile.ent.Payment;

import java.math.BigDecimal;

public class PaymentActivity extends AppCompatActivity {

    GridView paymentGrid;

    MediaController mediaController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ProgressDialog pd= new ProgressDialog(PaymentActivity.this);

        setContentView(R.layout.moplist_view);
//        getSupportActionBar().hide();

         paymentGrid = findViewById(R.id.payGrid);
         ProgressBar bar = findViewById(R.id.payProgressBar);
        bar.setVisibility(View.GONE);
        TextView txt = findViewById(R.id.progress_text);
        txt.setVisibility(View.GONE);

//        PromptController.getMyInstance(this);
//        this.setFinishOnTouchOutside(false);


        paymentGrid.setAdapter(new PaymentGridAdapter(paymentGrid,this));
        paymentGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*
                Payment selectedPayment = (Payment) paymentGrid.getAdapter().getItem(position);
                TransactionViewModelFactory.getTransaction().getPayment().add(selectedPayment);
                paymentGrid.setEnabled(false);
                HttpConnectionUtil util = new HttpConnectionUtil();
                Runnable run = () ->{  try{
                  util.sendExtposReq(CommandType.POST_SALE, TerminalDataSet.getCookie(), Xmlwriter.getPostSaleReqPayload());
                }catch (Exception e) {}};
                new Thread(run).start();
                bar.setVisibility(View.VISIBLE);

                videoView.start();*/
             }
        });

    }

    private void amountInputBuilder(Payment selectedPayment){
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Enter Amount");
            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            input.setRawInputType(Configuration.KEYBOARD_12KEY);
            alert.setView(input);
            final String amt;
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String  amount=  input.getText().toString();
                    selectedPayment.setAmount(new BigDecimal(amount));
                    return ;
                }
            });
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //Put actions for CANCEL button here, or leave in blank
                }
            });
        alert.show();
    }
    @Override
    protected void onStart() {
        super.onStart();


       /* Runnable keepRun =() -> {
            while (true){

                if(!videoView.isPlaying()){
                    videoView.start();
                }
            }
        };new Thread(keepRun).start();*/

    }
    @Override
    public void onBackPressed() {


    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
