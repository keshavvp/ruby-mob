package com.example.rubymobile;

import android.hardware.Camera;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
 import android.widget.ArrayAdapter;
 import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.rubymobile.com.example.adatper.MerchandiseSearchListAdapter;
import com.example.rubymobile.pres.IBackPressed;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.client.android.BeepManager;


import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.stream.Stream;


public class ScanFragment extends Fragment implements IBackPressed {

    static ScanFragment myInstance;
    public  static  ScanFragment getInstance(){
        if(myInstance == null){
            myInstance = new ScanFragment();
        }
        return  myInstance;
    }
    View view;
    SearchView searchView;

    public CameraSource cameraSource;
    private static BeepManager beep;
    ListView listView;
    ArrayList<String> list;
    ArrayAdapter<String > adapter;

    Camera camera;
    SurfaceView cameraView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view = inflater.inflate(R.layout.fragment_scan, container, false);
    }
    public void moveToMerchandise(){
        searchView.callOnClick();
     }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);


    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    }
    @Override
    public void onResume() {
        super.onResume();

     }

    private void startBarcodeReader() {
        beep = new BeepManager(getActivity());
        cameraView = (SurfaceView) getView().findViewById(R.id.camera_view);
        BarcodeDetector barcodeDetector =
                new BarcodeDetector.Builder(getContext())
                        .setBarcodeFormats(Barcode.CODE_128)//QR_CODE)
                        .build();
        //                .setRequestedPreviewSize(3840, 2160)
        cameraSource  = new CameraSource.Builder(getContext(), barcodeDetector).setRequestedPreviewSize(1600, 1200)
//                .setRequestedFps(2.0f)
                .setAutoFocusEnabled(true)
                .setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO)
                .build();


        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if(getView().isActivated()) {
                        cameraSource.start(cameraView.getHolder());
                    }

                } catch (IOException ie) {
                    Log.e("CAMERA SOURCE", ie.getMessage());
                }

            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

                if (cameraSource != null) {
                    cameraSource.release();
                    cameraSource.stop();

                 }
            }
        });
        listView = (ListView) getView().findViewById(R.id.list_view);
        MerchandiseSearchListAdapter searchAdpter = new MerchandiseSearchListAdapter(getContext(),listView);
        listView.setAdapter(searchAdpter);
        listView.setVisibility(View.INVISIBLE);

        searchView  = (SearchView)getView().findViewById(R.id.searchItems);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchAdpter.getFilter().filter(query);


                /*if(list.stream().anyMatch(query::equalsIgnoreCase)){
                    ad.getFilter().filter(query);

                }else{
                    Toast.makeText(getContext(), "No Match found",Toast.LENGTH_LONG).show();
                }*/
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                          searchAdpter.getFilter().filter(newText);

                return false;
            }
        }) ;
        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {

                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() != 0) {
                    searchView.post(new Runnable() {    // Use the post method of the TextView
                        public void run() {
                            searchView.setQuery(    // Update the TextView
                                    barcodes.valueAt(0).displayValue
                                    ,false);
                            searchView.callOnClick();
                            searchAdpter.notifyBArcodeReceived(barcodes.valueAt(0).displayValue);
                            beep.playBeepSoundAndVibrate();

                        }
                    }); try{
                        Thread.sleep(2000);
                    }catch (Exception e){}
                }
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                listView.setVisibility(View.INVISIBLE);
                return false;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
                listView.bringToFront();
                listView.setVisibility(View.VISIBLE);


            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if(cameraSource != null){
           try {
               cameraSource.release();
               cameraSource.stop();
           }catch (Exception e){

           }
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
         } else {

        }
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            startBarcodeReader();

            try {

                    cameraSource.start(cameraView.getHolder());


            } catch (IOException ie) {
                Log.e("CAMERA SOURCE", ie.getMessage());
            }
        } else {
           if(cameraSource!=null){
                cameraSource.stop();
               searchView.setIconified(true);
               searchView.onActionViewCollapsed();
           }
        }
    }
    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public boolean backPressed() {
        boolean callSuper = true;

        return callSuper;
    }
}
