package com.example.rubymobile;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.example.rubymobile.TTSHandler.TTSHandler;
import com.example.rubymobile.com.example.adatper.TabAdapter;
import com.example.rubymobile.controller.PromptController;
import com.example.rubymobile.data.ConfigDataViewModelFactory;
import com.example.rubymobile.data.HELPER.FileHelper;
import com.example.rubymobile.data.RefreshConfigData;
import com.example.rubymobile.data.TransactionViewModel;
import com.example.rubymobile.data.TransactionViewModelFactory;
import com.example.rubymobile.pres.IBackPressed;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    TTSHandler textToSpeech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);
        FileHelper.setContext(this);

        //
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        textToSpeech = new TTSHandler(this.getApplicationContext());

        RefreshConfigData dataModel = ViewModelProviders.of(this, new ConfigDataViewModelFactory(this.getApplication()))
                .get(RefreshConfigData.class);
//        dataModel.getConfiguration();
        TransactionViewModel mdoel = ViewModelProviders.of(this, new TransactionViewModelFactory(new TransactionViewModel()))
                .get(TransactionViewModel.class);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragment( CartFragment.getInstance(), "CART");
        adapter.addFragment(  HomeFragment.getInstance(), "HOME");
         adapter.addFragment(ScanFragment.getInstance(), "SCAN");
        viewPager.setOffscreenPageLimit(3);
        HomeFragment.getInstance().setTTS(textToSpeech);
        viewPager.setAdapter(adapter);



        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.carticon);
        tabLayout.getTabAt(1).setIcon(R.drawable.home);
        tabLayout.getTabAt(2).setIcon(R.drawable.scan2);
         View root = tabLayout.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(getResources().getColor(R.color.colorPrimary));
            drawable.setSize(2, 1);
            ((LinearLayout) root).setDividerPadding(10);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        textToSpeech.shutdown();
    }
    public void moveToMerchandise(){
        TabLayout.Tab t = tabLayout.getTabAt(2);
        tabLayout.selectTab(t);
        Fragment curFrag= getSupportFragmentManager().getFragments().get(t.getPosition());
        ((ScanFragment)curFrag).moveToMerchandise();

    }

    @Override
    public void onBackPressed() {
         Fragment curFrag= getSupportFragmentManager().getFragments().get(this.tabLayout.getSelectedTabPosition());
        boolean callSuper = false;
        if (curFrag != null && curFrag.isVisible()) {
           callSuper =  ((IBackPressed)curFrag).backPressed();
        }
        if(callSuper){
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println();
        tabLayout.getTabAt(1).select();
        PromptController.getMyInstance(this);
    }
}

