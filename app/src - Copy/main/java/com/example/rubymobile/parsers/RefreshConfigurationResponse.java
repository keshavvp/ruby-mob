package com.example.rubymobile.parsers;


import com.example.rubymobile.ent.Item;
import com.example.rubymobile.ent.ItemSet;
import com.example.rubymobile.ent.Payment;

import java.util.ArrayList;


public class RefreshConfigurationResponse  {

	private ArrayList<Item> pluList = new ArrayList<>();
	private ArrayList<Payment> mopList = new ArrayList<>();
	private ArrayList<ItemSet>  setList = new ArrayList<>();
	private ArrayList<Payment> subSetList = new ArrayList<>();
	private String logoName;
	private String logoUrl;
	private String logoDateModified;
	private String PostFinancialTxnFrequency;



	public String getPostFinancialTxnFrequency() {
		return PostFinancialTxnFrequency;
	}

	
	public String getLogoName() {
		return logoName;
	}
	public void setLogoName(String logoName) {
		this.logoName = logoName;
	}
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	public String getLogoDateModified() {
		return logoDateModified;
	}
	public void setLogoDateModified(String logoDateModified) {
		this.logoDateModified = logoDateModified;
	}
	public void addItem(Item item){
		pluList.add(item);
	}
	/**
	 * @return the pluList
	 */
	public ArrayList<Item> getPluList() {
		return pluList;
	}
	/**
	 * @param pluList the pluList to set
	 */
	public void setPluList(ArrayList<Item> pluList) {
		this.pluList = pluList;
	}

	public ArrayList<ItemSet> getSetList() {
		return setList;
	}
	public void setSetList(ArrayList<ItemSet> setList) {
		this.setList = setList;
	}
	public ArrayList<Payment> getSubSetList() {
		return subSetList;
	}
	public void setSubSetList(ArrayList<Payment> subSetList) {
		this.subSetList = subSetList;
	}
	public ArrayList<Payment> getMopList() {
		return mopList;
	}
	public void setMopList(ArrayList<Payment> mopList) {

		this.mopList = mopList;
	}
	public void setPostFinancialTxnFrequency(String freq) {
		PostFinancialTxnFrequency= freq;

	}

}
