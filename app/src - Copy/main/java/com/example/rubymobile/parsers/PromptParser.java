package com.example.rubymobile.parsers;


import com.example.rubymobile.ent.DisplayMessageType;
import com.example.rubymobile.ent.IPromptReq;
import com.example.rubymobile.ent.PrmptCommandType;
import com.example.rubymobile.ent.PromptReqType;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class PromptParser extends DefaultHandler{
	StringBuffer buffer = new StringBuffer();
	IPromptReq req;
	public IPromptReq getReq(){
		return req;
	}
	private boolean isFlash;
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException {
		buffer.delete(0, buffer.length());
		if(localName.equalsIgnoreCase("displayMessage")) {
			req = new DisplayMessageType();
			isFlash = true;
			req.setPromptID(Long.valueOf(attributes.getValue("promptID")));
			req.setCommandType(PrmptCommandType.FLASH);
			req.settimeOut(Integer.valueOf(attributes.getValue("timeOut")));
		}
		else if(localName.equalsIgnoreCase("promptRequest")) {
			req = new PromptReqType();
			isFlash = false;
			req.setPromptID(Long.valueOf(attributes.getValue("promptID")));
//			req.setCommandType(PrmptCommandType.FLASH);
			try {
				req.settimeOut(Integer.valueOf(attributes.getValue("timeOut")));
				req.setIsCancellable(Boolean.valueOf(attributes.getValue("cancellable")));
			}catch (Exception e){}


		}
		
		
	}
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException{
		if( localName.equalsIgnoreCase("header") )	 {
			req.setHeader(buffer.toString());
		}else if(localName.equalsIgnoreCase("message")) {
				req.getMsg().add((buffer.toString()));
		}
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		buffer.append(ch, start, length);
	}
}