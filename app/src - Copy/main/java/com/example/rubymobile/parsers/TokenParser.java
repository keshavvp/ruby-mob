package com.example.rubymobile.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class TokenParser extends DefaultHandler{
	static 	String token;
	static String appType;
	public static String getToken(){
		return token;
	}
	public static String getappType(){
		return appType;
	}

	StringBuffer buffer = new StringBuffer();
	private boolean istoken;
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException {
		buffer.delete(0, buffer.length());
		if(localName.equalsIgnoreCase("token")) {
			istoken = true;
			appType =attributes.getValue("applicationType");

		}
	}
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException{
		if(localName.equalsIgnoreCase("value")) {
			token = buffer.toString();
		}

	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		buffer.append(ch, start, length);
	}
}
