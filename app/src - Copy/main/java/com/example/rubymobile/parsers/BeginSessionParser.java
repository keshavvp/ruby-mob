package com.example.rubymobile.parsers;


import com.example.rubymobile.ent.TerminalDataSet;
import com.example.rubymobile.websocket.WsController;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class BeginSessionParser extends DefaultHandler {
	String extSessionID;
	boolean isres;
	String tillStatus;
	static  String failurereason;
	StringBuffer buffer = new StringBuffer();
	public  static boolean  isSuccess ;
	private boolean isTillValue;

	public String getTillStatus() {
		return tillStatus;
	}

	public void setTillStatus(String tillStatus) {
		this.tillStatus = tillStatus;
	}

	public static String getFailurereason() {
		return failurereason;
	}

	public static void setFailurereason(String failurereason) {
		BeginSessionParser.failurereason = failurereason;
	}

	public static boolean isIsSuccess() {
		return isSuccess;
	}

	public static void setIsSuccess(boolean isSuccess) {
		BeginSessionParser.isSuccess = isSuccess;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException {
		buffer.delete(0, buffer.length());
		if(localName.equalsIgnoreCase(VistConstants.TILL_STATUS)) {
			isTillValue = true;			 
		}else if(localName.equalsIgnoreCase(VistConstants.RESULT)) {
			isres = true;
		}else if(localName.equalsIgnoreCase(VistConstants.FAILURE)&& isres ==true) {
			isSuccess = false;

			failurereason = attributes.getValue("reason");
		}
		else if(localName.equalsIgnoreCase(VistConstants.SUCCESS)&& isres ==true) {
			isSuccess = true;
		}

		if((isTillValue)&&(localName.equalsIgnoreCase(VistConstants.OPEN) || localName.equalsIgnoreCase(VistConstants.CLOSE)) ){

			tillStatus = localName;		 
		}
	}
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException{
		if(localName.equalsIgnoreCase(VistConstants.EXT_SESSION_ID)) {
			extSessionID = buffer.toString();
			TerminalDataSet.setextSessionID(extSessionID);
			 }
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		buffer.append(ch, start, length);
	}
}
