package com.example.rubymobile.parsers;

import com.example.rubymobile.ent.PostedTransaction;
import com.example.rubymobile.ent.ReceiptText;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

public class PostFinResponseParser extends DefaultHandler {

	StringBuffer buffer = new StringBuffer();
	private boolean isres;
	private boolean isSuccess;
	private String failurereason;
	private Boolean isTxnComplete;
	PostedTransaction postTxn;
	private boolean isTax;
	private boolean receiptSec;
	private boolean isHeaderTxt;
	private boolean isBodyText;
	private boolean isFooter;	ArrayList<ReceiptText> headerText;
	ArrayList<ReceiptText> bodyText;
	ArrayList<ReceiptText> footerText;

 	String alignment ;

	public PostedTransaction getTxn(){
		return postTxn;
	}
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException {
		buffer.delete(0, buffer.length());
		if(localName.equalsIgnoreCase(VistConstants.POST_FINANCIAL_TXN_RESP)) {
			postTxn = new  PostedTransaction();
		}
		else if(localName.equalsIgnoreCase(VistConstants.RESULT)) {
			isres = true;
		}else if(localName.equalsIgnoreCase(VistConstants.FAILURE)&& isres ==true) {
			isSuccess = false;
			isres =false;
			failurereason = attributes.getValue("reason");
			postTxn.setResSuccess(isSuccess);
			postTxn.setFailurereason(failurereason);

		}
		else if(localName.equalsIgnoreCase(VistConstants.POSTED_EXTERNAL_SALE)) {
		 
			isTxnComplete = Boolean.valueOf(attributes.getValue("isSaleCompleted"));
			postTxn.setSaleCompleted(isTxnComplete);

		}
		else if(localName.equalsIgnoreCase("tax")) {
			 isTax = true;
		}
		else if(localName.equalsIgnoreCase("formattedReceiptText")) {
			receiptSec = true;
 		}
		else if(localName.equalsIgnoreCase("header")) {
			isHeaderTxt = true;
			headerText = new ArrayList<>();


		}
		else if(localName.equalsIgnoreCase("body")) {
			isHeaderTxt = false;
			isBodyText =true;
			bodyText = new ArrayList<>();
		}
		else if(localName.equalsIgnoreCase("footer")) {
			isHeaderTxt = false;
			isBodyText =false;
			isFooter = true;
			footerText = new ArrayList<>();
		}
		else if(localName.equalsIgnoreCase("textLine")) {

			alignment = attributes.getValue("alignment");

		}
	}
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException{
		if(localName.equalsIgnoreCase("value")) {
 		}
		else if(localName.equalsIgnoreCase("amount") ) {
			if(isTax) {
			 isTax = false;
			 postTxn.setEvaluatedTotalTaxAmt(buffer.toString());
			}
		}
		else if(localName.equalsIgnoreCase(VistConstants.SUCCESS)&& isres ==true) {
			isSuccess = true;
			isres =false;
			postTxn.setResSuccess(isSuccess);
		}
		else if(localName.equalsIgnoreCase(VistConstants.TOTAL) ) {
			 postTxn.setEvaluatedTotal(buffer.toString());
			}
		else if(localName.equalsIgnoreCase("textLine")) {

			if(!buffer.toString().isEmpty()) {
				ReceiptText txt =new ReceiptText();
				txt.setAlignment(alignment);
				txt.setText(buffer.toString());
				if(isHeaderTxt) {
					headerText.add(txt);
				}else if(isBodyText) {
					bodyText.add(txt);
				}else if(isFooter) {
					footerText.add(txt);
				}
			}

		}
		else if(localName.equalsIgnoreCase("formattedReceiptText")) {
			postTxn.setHeader(headerText);
			postTxn.setBody(bodyText);
			postTxn.setFooter(footerText);


		}
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		buffer.append(ch, start, length);
	}

}
