package com.example.rubymobile.parsers;

import android.util.Log;


import com.example.rubymobile.controller.PromptController;
import com.example.rubymobile.data.CommandType;
import com.example.rubymobile.data.ConfigDataViewModelFactory;
import com.example.rubymobile.data.HELPER.FileHelper;
import com.example.rubymobile.ent.IPromptReq;
import com.example.rubymobile.ent.ItemSet;
import com.example.rubymobile.ent.ItemSubSetItem;
import com.example.rubymobile.ent.ItemSubset;
import com.example.rubymobile.ent.PrmptCommandType;
import com.example.rubymobile.ent.PromptReqType;
import com.example.rubymobile.handler.BeginSessionResponseHandler;
import com.example.rubymobile.handler.PostFinTxnResponseHandler;
import com.example.rubymobile.handler.SuspendHandler;
import com.example.rubymobile.network.HttpConnectionUtil;

import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class ParserFactory {
    private static SAXParser saxParser;
    private static  SAXParserFactory sax;
    public static void parseValidate(String xml){
        try{
             sax = SAXParserFactory.newInstance();
             saxParser = sax.newSAXParser();
             ValidateResponseParser bs = new ValidateResponseParser();
             InputSource in = new InputSource(new StringReader(xml.trim()));
             saxParser.parse(in, bs);
        }catch (Exception e) {
            Log.d("ParserFactory","validate parse error");
        }
    }
    public static void parse(CommandType cmd, String responseXml) throws Exception{
        if(cmd ==CommandType.BEGIN_SESSION){
           try {
               BeginSessionParser bs = new BeginSessionParser();
               InputSource in = new InputSource(new StringReader(responseXml.trim()));
               sax = SAXParserFactory.newInstance();
               saxParser = sax.newSAXParser();
               saxParser.parse(in, bs);
               BeginSessionResponseHandler handler = new BeginSessionResponseHandler(bs);{
                   handler.handle();
               }
           }
           catch (Exception e){ throw e;}
        }
        if(cmd ==CommandType.REFRESH_CONFIG){
            try {
                boolean t =FileHelper.saveToFile(responseXml.trim());

                RefreshConfigurationResponse res = new RefreshConfigurationResponse();
                RefreshConfigResponseSaxParser bs = new RefreshConfigResponseSaxParser(res);
                InputSource in = new InputSource(new StringReader(responseXml.trim()));
                sax = SAXParserFactory.newInstance();
                saxParser = sax.newSAXParser();
                saxParser.parse(in, bs);

                syncImages(res.getSetList());
            }
            catch (Exception e){ throw e;}
        }
        if(cmd ==CommandType.POST_SALE){
            try {
                PostFinResponseParser postParser = new PostFinResponseParser();
                InputSource in = new InputSource(new StringReader(responseXml.trim()));
                sax = SAXParserFactory.newInstance();
                saxParser = sax.newSAXParser();
                saxParser.parse(in, postParser);
                PostFinTxnResponseHandler handler = new PostFinTxnResponseHandler(postParser.getTxn());{
                    handler.handle();

                }
            }
            catch (Exception e){ throw e;
            }
        }
        if(cmd ==CommandType.VOID ||cmd ==CommandType.SUSP_TXN){
            try {
                SuspendParser susp = new SuspendParser();
                InputSource in = new InputSource(new StringReader(responseXml.trim()));
                sax = SAXParserFactory.newInstance();
                saxParser = sax.newSAXParser();
                saxParser.parse(in, susp);
                String speakTxt = "";
                if(cmd ==CommandType.VOID){
                    speakTxt = "Sale Voided";
                }else if(cmd ==CommandType.SUSP_TXN){
                    speakTxt = "Sale Suspended";


                }
                SuspendHandler handler = new SuspendHandler(susp.isSuccess(),speakTxt);{
                    handler.handle();
                }
            }
            catch (Exception e){ throw e;
            }
        }



    }
    public static IPromptReq createPromptReq(String xml){
        PromptParser parser = null;
            try {
                parser = new PromptParser();
                InputSource in = new InputSource(new StringReader(xml.trim()));
                sax = SAXParserFactory.newInstance();;
                saxParser = sax.newSAXParser();
                saxParser.parse(in, parser);
                 PromptController.handlePorompt(parser.getReq());

            }
            catch (Exception e){
                Log.d("Promptreq parse error{}",e.getMessage());
            }
        return parser.getReq();
        }

    private static void syncImages(ArrayList<ItemSet> itemsetList2) {
        HttpConnectionUtil con = new HttpConnectionUtil();

        for(ItemSet set : itemsetList2) {
            for(ItemSubset sub : set.getItemSubsets()) {
                for(ItemSubSetItem subITems : sub.getsubSetItems()) {
                    Runnable run = () -> {
                        String imgName = subITems.getIconName();
                        String Url = subITems.getIconUrl();
                        con.sendImageReq(imgName,Url);
                    };
                    new Thread(run).start();

                }
            }
        }

    }
}
