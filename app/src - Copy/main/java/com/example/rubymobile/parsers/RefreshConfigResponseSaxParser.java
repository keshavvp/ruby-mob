package com.example.rubymobile.parsers;



import com.example.rubymobile.ent.Item;
import com.example.rubymobile.ent.ItemSet;
import com.example.rubymobile.ent.ItemSubSetItem;
import com.example.rubymobile.ent.ItemSubset;
import com.example.rubymobile.ent.Payment;
import com.example.rubymobile.network.HttpConnectionUtil;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.math.BigDecimal;
import java.util.ArrayList;

public class RefreshConfigResponseSaxParser extends DefaultHandler {
	RefreshConfigurationResponse res;
	public RefreshConfigResponseSaxParser(RefreshConfigurationResponse res2) {
		this.res = res2;
	}
	StringBuffer buffer = new StringBuffer();

	 
	ArrayList<Item> itemList = new ArrayList<Item>();
	Item  pluItem;
	boolean isItem ;boolean ismop ;boolean isItemSet ; boolean isItemSubSet ;
	ArrayList<Payment> mopList = new ArrayList<>();
	Payment mop;
	ArrayList<ItemSet> itemsetList = new ArrayList<>();
	ArrayList<ItemSubset> ItemSubsetList= new ArrayList<>();
	ArrayList<ItemSubSetItem> itemsubSetitemsList= new ArrayList<>();
	ItemSubset subSet;
	ItemSubSetItem subsetItem ;
	ItemSet set;// = new ItemSet();
	
	
	 @Override
	   public void startElement(String uri, String localName, String qName, Attributes attributes)
			   throws SAXException {
		 buffer.delete(0, buffer.length());
		 if (localName.equalsIgnoreCase("salesProcessorRtn")) {
			 System.out.println("salesProcessorRtn: " );
		 } else if (localName.equalsIgnoreCase("refreshConfigurationResp")) {
			 res.setPostFinancialTxnFrequency(attributes.getValue(VistConstants.TXN_FREQUENCY));

		 } else if (localName.equalsIgnoreCase(VistConstants.Sellable)) {




		 } 
		 else if (localName.equalsIgnoreCase(VistConstants.ITEM)) {
			 if(isItemSubSet) {
				 subsetItem = new ItemSubSetItem();
			 }
		 } 
		 
		  if (localName.equalsIgnoreCase("plu")) {
			if(!isItemSet) {
				isItem  = true;
			}
			 pluItem = new Item();
			 String freq	= attributes.getValue("triggerPostFinancialTxn");
			 pluItem.setTriggerPostFinancialTxn(Boolean.valueOf(freq));
		 }
		 else if (localName.equalsIgnoreCase("price")) {
			 String currency = attributes.getValue("currency");
			 String symbol=  attributes.getValue("symbol");
			 pluItem.setSymbol(symbol);
			 pluItem.setcurrency(currency);
		 }
		 else if (localName.equalsIgnoreCase(VistConstants.MOP)) {
			 ismop = true;
			 mop = new Payment();
			 String sysid = attributes.getValue(VistConstants.SYSID);
			 String symbol=  attributes.getValue(VistConstants.MOP_CODE);
			 mop.setSysid(sysid);
			 mop.setMopCode(symbol);
		 } 
		 else if (localName.equalsIgnoreCase("itemSet")) {
			 set = new ItemSet();
			 isItemSet = true;
			
//			 
		 }
		 else if (localName.equalsIgnoreCase("itemSubset")) {
			 subSet = new ItemSubset();
			 isItemSubSet = true;
//			 
		 }
		 else if ((localName.equalsIgnoreCase("logo") ||localName.equalsIgnoreCase(VistConstants.DISPLAY_ICON))) {
			 String dateModified =attributes.getValue("dateModified");
			 String iconName =attributes.getValue("id");
			 String iconUrl =attributes.getValue("url");
			 if(localName.equalsIgnoreCase("logo")) {
				res.setLogoDateModified(dateModified);
				res.setLogoName(iconName);
				res.setLogoUrl(iconUrl);
			 }
			 else if(isItemSet && !isItemSubSet) {
				 set.setDateModified(dateModified);
				 set.setIconName(iconName);
				 set.setIconUrl(iconUrl);
			 }
			 else if(isItemSubSet) {
				 subsetItem.setDateModified(dateModified);
				 subsetItem.setIconName(iconName);
				 subsetItem.setIconUrl(iconUrl);
			 }
			 
		 } 
		 

	 }

	   @Override
	   public void endElement(String uri, 
	      String localName, String qName) throws SAXException {
	      
		  if (localName.equalsIgnoreCase("salesProcessorRtn")) {
			  res.setPluList(itemList);
			  res.setMopList(mopList);
			  res.setSetList(itemsetList);
//			  syncImages(itemsetList);
		  }else if (localName.equalsIgnoreCase("plu")) {
	         System.out.println("End Element :" + qName);
	         itemList.add(pluItem);
	         isItem= false;
	      }
		  else if (localName.equalsIgnoreCase(VistConstants.ITEM)) {
			  itemsubSetitemsList.add(subsetItem);
			 

			 } 
	      else if (localName.equalsIgnoreCase(VistConstants.MOP)) {
	    	  mopList.add(mop);
				 ismop = false;
	      }
	      else if (localName.equalsIgnoreCase("itemSet")) {
	    	  itemsetList.add(set);
	    	  itemsubSetitemsList = new ArrayList<ItemSubSetItem>();
	    	  isItemSet=false;
	      }
	      else if (localName.equalsIgnoreCase("itemSubset")) {
				 isItemSubSet = false;
				 subSet.getsubSetItems().addAll(itemsubSetitemsList);
				 ItemSubsetList.add(subSet);
				 set.setItemSubsets(ItemSubsetList);
				 ItemSubsetList  = new ArrayList<ItemSubset>();
				 
//				 
			 }
	       
	      else if (localName.equalsIgnoreCase("upc")) {
	    	  if(isItem) {
	    		  pluItem.setUPC(buffer.toString());
	    	  }else if(isItemSubSet) {
	    		  subsetItem.setUPC(buffer.toString());
	    	  }
	    	  
	      }
	      else if (localName.equalsIgnoreCase("displayName")) {
	    	 if(isItemSubSet) {
	    		 subsetItem.setDisplayName(buffer.toString());
	    	 }
	    	  
	      }
		  
	      else if (localName.equalsIgnoreCase("modifier")) {
	    	  if(isItem) {
	    	  pluItem.setModifier(buffer.toString());
	      } else if(isItemSubSet) {
	    		  subsetItem.setModifier(buffer.toString());
	    	  }
	      }
	      else if (localName.equalsIgnoreCase("name")) {
	    	  if(isItem){
	    		  pluItem.setName(buffer.toString());
	    	  }else if(ismop) {
	    		  mop.setName(buffer.toString());
	    	  }else if(isItemSet && !isItemSubSet) {
	    		  set.setName(buffer.toString());
	    	  }
	    	  else if(isItemSubSet) {
	    		  subSet.setName(buffer.toString());
	    	  }
	      }

	      else if (localName.equalsIgnoreCase("price")) {
	    	  pluItem.setAmount(new BigDecimal(buffer.toString()));
	    	  
	      }
	      else if (localName.equalsIgnoreCase("colorCode")) {
				if(isItemSet) {
					set.setColorCode(buffer.toString());
				}
//				 
			 } 
	      else if (localName.equalsIgnoreCase("hotItem")) {
				if(isItemSet) {
					subsetItem.setHotItem(Boolean.valueOf(buffer.toString()));
				}
//				 
			 } 
		  
	   }

	   @Override
	   public void characters(char ch[], int start, int length) throws SAXException {
		   buffer.append(ch, start, length);
	   }
}
