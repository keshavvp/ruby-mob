package com.example.rubymobile.handler;

import com.example.rubymobile.com.example.adatper.CustomAddedItemDataAdapter;
import com.example.rubymobile.data.TransactionViewModelFactory;

public class SuspendHandler {

    boolean isSuccess;
    static CustomAddedItemDataAdapter updaterAdapter;
    String speakTxt;
    public SuspendHandler(boolean isSuccess,String speakTxt){
        this.isSuccess = isSuccess; this.speakTxt =speakTxt;
    }
    public static void setUpdaterAdapter(CustomAddedItemDataAdapter ad){
        updaterAdapter = ad;
    }
    public void handle() throws Exception{
        if(isSuccess){
            TransactionViewModelFactory.getTransaction().clear();
            updaterAdapter.notifySaleResponse(true,null,speakTxt);
        }
    }


}
