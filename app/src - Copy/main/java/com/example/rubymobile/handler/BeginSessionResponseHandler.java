package com.example.rubymobile.handler;


import com.example.rubymobile.data.CommandType;
import com.example.rubymobile.data.HELPER.FileHelper;
import com.example.rubymobile.ent.TerminalDataSet;
import com.example.rubymobile.network.HttpConnectionUtil;
import com.example.rubymobile.parsers.BeginSessionParser;
import com.example.rubymobile.websocket.WsController;
import com.example.rubymobile.write.Xmlwriter;

public class BeginSessionResponseHandler {
    BeginSessionParser data;
    public  BeginSessionResponseHandler(BeginSessionParser data){
        this.data = data;
    }
    public void handle() throws Exception {
        if (!BeginSessionParser.isSuccess) {
            throw new Exception("login failed " + BeginSessionParser.getFailurereason());
        } else {
            WsController ctrl = new WsController();
            ctrl.connect();

            HttpConnectionUtil con = new HttpConnectionUtil();
            if (data.getTillStatus().equalsIgnoreCase("CLOSE")) {
                // HttpConnectionUtil con = new HttpConnectionUtil();
                con.sendExtposReq(CommandType.OPEN_CASHIER, TerminalDataSet.getCookie(), Xmlwriter.getOpenCashierXmlPayload());
            }
            if(!FileHelper.isRefreshFilePresent()){
                con.sendExtposReq(CommandType.REFRESH_CONFIG, TerminalDataSet.getCookie(), Xmlwriter.getRefreshConfigXmlPayload());
            }


        }
    }
     }
