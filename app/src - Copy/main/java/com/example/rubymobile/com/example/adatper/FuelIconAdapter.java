package com.example.rubymobile.com.example.adatper;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.rubymobile.R;

public class FuelIconAdapter extends BaseAdapter {
    private Context mContext;

    // Constructor
    public FuelIconAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)mContext.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
       View view= inflater.inflate(R.layout.fuel_icon,null);
        view.setBackgroundColor(Color.TRANSPARENT);
      TextView im = view.findViewById(R.id.fpPosNum);
      im.setText(String.valueOf(position + 1));

      return view;
     }

    // Keep all Images in array
    public Integer[] mThumbIds = {
            R.drawable.fuelicon, R.drawable.fuelicon,
            R.drawable.fuelicon, R.drawable.fuelicon,
            R.drawable.fuelicon, R.drawable.fuelicon,
            R.drawable.fuelicon, R.drawable.fuelicon,
            R.drawable.fuelicon, R.drawable.fuelicon,
            R.drawable.fuelicon,
            R.drawable.fuelicon,
            R.drawable.fuelicon,
            R.drawable.fuelicon,
            R.drawable.fuelicon,
            R.drawable.fuelicon,
    };
}
