package com.example.rubymobile.com.example.adatper;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;


import com.example.rubymobile.CartFragment;
import com.example.rubymobile.HomeFragment;
import com.example.rubymobile.R;
import com.example.rubymobile.controller.PromptController;
import com.example.rubymobile.data.HELPER.FileHelper;
import com.example.rubymobile.data.TransactionViewModelFactory;
import com.example.rubymobile.ent.Item;
import com.example.rubymobile.ent.ItemLine;
import com.example.rubymobile.ent.ItemSubSetItem;
import com.example.rubymobile.ent.PostedTransaction;
import com.example.rubymobile.ent.PrepayItemLine;
import com.example.rubymobile.ent.ReceiptText;
import com.example.rubymobile.ent.StringFormat;
import com.example.rubymobile.handler.PostFinTxnResponseHandler;
import com.example.rubymobile.handler.SuspendHandler;
import com.example.rubymobile.parsers.RefreshConfigurationResponse;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class CustomAddedItemDataAdapter extends BaseAdapter {
    RefreshConfigurationResponse configurationData;
    ArrayList<Item> availablePlu;
    FragmentActivity contxt;
    ListView list;
    AlertDialog receiptDialog;
    View cartView;
    double total =0;TextView txnQuantity;
    int totalQty=0;
    public CustomAddedItemDataAdapter(FragmentActivity contxt, RefreshConfigurationResponse configurationData, ListView list, View cartView){

        this.configurationData = configurationData;
        this.contxt = contxt;
         this.cartView = cartView;
         this.availablePlu = configurationData.getPluList();
         this.list = list;
        txnQuantity = cartView.findViewById(R.id.qty);

        list.setVisibility(View.VISIBLE);
        PostFinTxnResponseHandler.setUpdaterAdapter(this);
        MerchandiseSearchListAdapter.setUpdaterAdapter(this);
        FuelSaleController.setUpdaterAdapter(this);
        SuspendHandler.setUpdaterAdapter(this);

        }

    @Override
        public int getCount() {
            return TransactionViewModelFactory.getTransaction().getItems().size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }
    public void notifySaleResponse(boolean isSaleCompleted,PostedTransaction txn, String speakTxt) {
        {
           Runnable clearThread= ()->  contxt.runOnUiThread(new Runnable() {
                @Override
                public void run()  {

                    notifyDataSetChanged();
                    TextView tot =cartView.findViewById(R.id.totalPrice);
                    TextView taxPrice =cartView.findViewById(R.id.taxPrice);
                    PromptController.clear();

                    taxPrice.setText(TransactionViewModelFactory.getTransaction().getTax());
                    tot.setText(TransactionViewModelFactory.getTransaction().getTotal());
                    if( txn!= null && txn.getFailurereason() != null && !txn.getFailurereason().equals("transactionInProgress")){
                        HomeFragment.getInstance().speak("TRANSACTION FAILED");
                        Snackbar.make(contxt.getWindow().getDecorView(),speakTxt,Snackbar.LENGTH_SHORT).show();
                        return;
                    }else  if(txn!= null && txn.getFailurereason() != null &&  txn.getFailurereason().equals("transactionInProgress")){
                        Snackbar.make(contxt.getWindow().getDecorView(),speakTxt,Snackbar.LENGTH_SHORT).show();
                        return;
                    }

                /*    notifyDataSetChanged();
                     TextView tot =cartView.findViewById(R.id.totalPrice);
                    TextView taxPrice =cartView.findViewById(R.id.taxPrice);

                    taxPrice.setText(TransactionViewModelFactory.getTransaction().getTax());
                    tot.setText(TransactionViewModelFactory.getTransaction().getTotal());
*/
                    if(isSaleCompleted){
                        HomeFragment.getInstance().speak(speakTxt);
                        HomeFragment.getInstance().showSnakBar(speakTxt);

//                        Toast.makeText(contxt,"Sale completed!",Toast.LENGTH_SHORT);
                           if(txn!=null && txn.getHeader()!= null && !txn.getHeader().isEmpty() ){
                           displayReceipt(txn);
                       }
                        CartFragment.getInstance().backPressed();
                    }

                }

           });
            new Thread(clearThread).start();
       /* TextView totalStr = bottomSheet.findViewById(R.id.totalPrice);
        totalStr.setText(String.valueOf(total));*/
        }
    }

    private void displayReceipt(PostedTransaction txn) {

        ArrayList<ReceiptText> header= txn.getHeader();
        ArrayList<ReceiptText> body= txn.getBody();
        ArrayList<ReceiptText> footer= txn.getFooter();
        View receipt = contxt.getLayoutInflater().inflate(R.layout.receipt_text_view,null);
        EditText headerT =  receipt.findViewById(R.id.header);
        EditText bodyT =  receipt.findViewById(R.id.body);
        EditText footrT =  receipt.findViewById(R.id.footer);


        StringBuilder reptStr = new StringBuilder();
        for(ReceiptText recptText : header) {
             if(recptText.getAlignment().equalsIgnoreCase("LEFT")) {
                reptStr.append(StringFormat.formatLeft(recptText.getText(), 40) + "\n");
            } else if(recptText.getAlignment().equalsIgnoreCase("RIGHT")) {
                 reptStr.append(StringFormat.formatRight(recptText.getText(), 40) + "\n");

            } else {
                reptStr.append(StringFormat.formatCenter(recptText.getText(), 40) + "\n");
            }

        }
        headerT.setText(reptStr);reptStr = new StringBuilder();
        for(ReceiptText recptText : body) {
             if(recptText.getAlignment().equalsIgnoreCase("LEFT")) {
                 reptStr.append(StringFormat.formatLeft(recptText.getText(), 40) +"\n" );

            } else if(recptText.getAlignment().equalsIgnoreCase("RIGHT")) {
                 reptStr.append(StringFormat.formatRight(recptText.getText(), 40) + "\n");

             } else {
                 reptStr.append(StringFormat.formatCenter(recptText.getText(), 40) + "\n");

            }

        }
        bodyT.setText(reptStr);reptStr = new StringBuilder();
        for(ReceiptText recptText : footer) {

            if(recptText.getAlignment().equalsIgnoreCase("LEFT")) {
                reptStr.append(StringFormat.formatLeft(recptText.getText(), 40) + "\n");



            } else if(recptText.getAlignment().equalsIgnoreCase("RIGHT")) {
                reptStr.append(StringFormat.formatRight(recptText.getText(), 40) + "\n");

            } else {
                reptStr.append(StringFormat.formatCenter(recptText.getText(), 40) + "\n");


            }


        }

        footrT.setText(reptStr);
        reptStr = new StringBuilder();
         AlertDialog.Builder alert =new AlertDialog.Builder(contxt);



        alert.setView(receipt);

         alert.setPositiveButton("Print", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                receiptDialog.dismiss();
            }});
        receiptDialog =  alert.create();
        receiptDialog.show();         Log.d("receipt",reptStr.toString());

    }


    public void notifyDataSetChanged() {
        total=0;
        totalQty =0;
        super.notifyDataSetChanged();
        list.setSelection(TransactionViewModelFactory.getTransaction().getItems().size() -1);
        updateTotAndQty();
        }

    private void updateTotAndQty() {
        TextView totalAmt = cartView.findViewById(R.id.totalPrice);
        int quantity = 0;
        ArrayList<ItemLine> items = TransactionViewModelFactory.getTransaction().getItems();
        String amount="0.0";
        String sym = "$";
        for(ItemLine itm : items ){
            if(itm instanceof ItemSubSetItem) {
                quantity +=  itm.getQty();
                for (Item availItm : availablePlu) {

                    if (availItm.getUPC().equals(itm.getUPC()) && availItm.getModifier().equals(itm.getModifier())) {
                        amount = availItm.getAmount().toString();
                        sym = availItm.getSymbol();
                        break;
                    }
                }
            }else{
                quantity = quantity+1;
                amount = itm.getAmount();
            }
            total+=Double.valueOf(amount);
        }
        String formatedTotal = String.format("%.2f",total);
        totalAmt.setText(sym + formatedTotal);
        totalQty = quantity;
        txnQuantity.setText(String.valueOf(quantity));
    }

    @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflator =(LayoutInflater)  contxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View listView = inflator.inflate(R.layout.cartlist,null);
            ImageView image = listView.findViewById(R.id.pluImage);
            TextView pluName = listView.findViewById(R.id.pluName);
//            TextView desc = listView.findViewById(R.id.pluDesc);
            TextView price = listView.findViewById(R.id.price);
            ItemLine item =  TransactionViewModelFactory.getTransaction().getItems().get(position);
            if((item.getIconName()!=null) &&! (item instanceof PrepayItemLine)){
//                new File(contxt.getExternalFilesDir(null),"/RubyMobile/"  );//.mkdir();

                File file = new File(contxt.getExternalFilesDir("RubyMobile"),item.getIconName());
//                File f =new File( FileHelper.path+"/images/"+item.getIconName());
                image.setImageURI(Uri.fromFile(file));
            }
            else{
                image.setImageResource(R.drawable.fuelicon);
            }

//            image.setImageResource(R.drawable.burger1);
            pluName.setLeft(1);
//            pluName.setText("Item F                   $9.99");
            pluName.setText(item.getDisplayName());
//            desc.setText(item.getUPC());
            Button negBtn = listView.findViewById(R.id.negativeBtn);
            TextView qty = listView.findViewById(R.id.itmQtyValue);
            negBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int quantity = Integer.valueOf(item.getQty());
                    if(quantity>0){
                        --quantity;
                        --totalQty;
                    }else {
                        quantity =0;
                    }
                    item.setQty(quantity);
                    qty.setText(String.valueOf(quantity));
                    txnQuantity.setText(String.valueOf(totalQty));


                }
            });

            Button plusBtn = listView.findViewById(R.id.positiveBtn);
            plusBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int quantity = Integer.valueOf(item.getQty());
                    if(quantity<99){
                        ++quantity;
                       ++totalQty;
                    }
                    item.setQty(quantity);
                    qty.setText(String.valueOf(quantity));
                    txnQuantity.setText(String.valueOf(totalQty));

                }
            });

            if(item instanceof PrepayItemLine){
                LinearLayout lay = listView.findViewById(R.id.qtyViw);
                lay.setVisibility(View.INVISIBLE);
            }
            String amount="";
            String sym="";
             for(Item itm : availablePlu){
                 if((item instanceof ItemSubSetItem)) {
                     if (itm.getUPC().equals(item.getUPC()) && itm.getModifier().equals(item.getModifier())) {
                         amount = itm.getAmount().toString();
                         sym = itm.getSymbol();
                         qty.setText(String.valueOf(item.getQty()));

                         break;
                     }
                 }else
                 {
                     amount = item.getAmount();
                     sym = item.getSym();
                     NumberFormat nf =  NumberFormat.getNumberInstance();
                     nf.setMaximumFractionDigits(2);
                     nf.setMinimumFractionDigits(2);
                     amount = nf.format(Double.valueOf(amount));
                     break;
                 }
            }
             price.setText(sym + amount);

//             list.setSelection(TransactionViewModelFactory.getTransaction().getItems().size() - 1);

       /* list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                Button btn =cartView.findViewById(R.id.delete);
                btn.setText("delet");
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ArrayList<ItemLine> items = TransactionViewModelFactory.getTransaction().getItems();
                        items.remove(position);
                        notifyDataSetChanged();
                    }
                });
                return false;
            }
        });*/

     /*        String formatedTotal = String.format("%.2f",total);
            totalAmt.setText(new String(sym +"  "+formatedTotal));*/
 //            bottomSheetTotal(sym);
//            TransactionViewModelFactory.getTransaction().setItems(addedItems);


//            price.setText(addedItems.get(position).get());
            return listView;
        }

   /* private void bottomSheetTotal(String sym) {
        TextView totalStr = bottomSheet.findViewById(R.id.totalPrice);
        String formatedTotal = String.format("%.2f",total);
        totalStr.setText(new String(sym +"  "+formatedTotal));
    }*/
}

